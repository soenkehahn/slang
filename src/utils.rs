pub mod errors;
pub mod map;
pub mod mvar;
pub mod pretty;
pub mod unique_names;

pub fn fold_left_first<Element, I, F>(iter: I, mut f: F) -> Option<Element>
where
  I: Iterator<Item = Element>,
  I: DoubleEndedIterator,
  F: FnMut(Element, Element) -> Element,
{
  let mut iter = iter.rev();
  match iter.next() {
    None => None,
    Some(first) => {
      let mut acc = first;
      for next in iter {
        acc = f(next, acc);
      }
      Some(acc)
    }
  }
}

#[allow(unused_macros)]
macro_rules! nyi {
  ($expr:expr) => {{
    eprintln!("NYI:\n{:#?}\n", $expr);
    panic!("nyi");
  }};
}

#[cfg(test)]
mod test {
  use super::*;

  mod fold_left_first {
    use super::*;

    fn fold_function(next: String, acc: String) -> String {
      format!("{} # ({})", next, acc)
    }

    #[test]
    fn returns_none_for_empty_iterators() {
      let vec = vec![];
      let result = fold_left_first(vec.into_iter(), fold_function);
      assert_eq!(result, None);
    }

    #[test]
    fn works_for_singleton_iterators() {
      let vec = vec!["initial".to_string()];
      let result = fold_left_first(vec.into_iter(), fold_function);
      assert_eq!(result, Some("initial".to_string()));
    }

    #[test]
    fn works_for_multiple_elements() {
      let vec = vec!["a", "b", "c"].into_iter().map(|x| x.to_string());
      let result = fold_left_first(vec, fold_function);
      assert_eq!(result, Some("a # (b # (c))".to_string()));
    }
  }
}
