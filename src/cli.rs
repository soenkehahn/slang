use clap::{App, Arg};
use std::path::PathBuf;

#[derive(Debug, PartialEq, Eq)]
pub struct Args {
  pub input: PathBuf,
  pub debug: bool,
}

impl Args {
  pub fn new<I: Iterator<Item = String>>(args: I) -> Args {
    let matches = App::new("slang")
      .version("1.0")
      .about("Sönke language compiler")
      .arg(Arg::with_name("input").required(true))
      .arg(Arg::with_name("debug").long("debug"))
      .get_matches_from(args);
    Args {
      input: PathBuf::from(matches.value_of("input").unwrap()),
      debug: matches.is_present("debug"),
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;

  impl<'a> Args {
    fn test(args: Vec<&'a str>) -> Args {
      Args::new(args.into_iter().map(|x| x.to_string()))
    }
  }

  #[test]
  fn allows_debug_flag() {
    assert!(Args::test(vec!["slang", "foo", "--debug"]).debug);
  }

  #[test]
  fn parses_single_arguments_as_interpreted_mode() {
    assert_eq!(
      Args::test(vec!["slang", "foo"]),
      Args {
        input: PathBuf::from("foo"),
        debug: false,
      }
    );
  }
}
