#![cfg_attr(not(feature = "dev"), deny(warnings))]
#![cfg_attr(
  feature = "dev",
  allow(unused_variables, unused_imports, dead_code, unreachable_code)
)]

#[macro_use]
mod utils;

mod builtins;
mod cli;
mod frontend;
mod interpreter;

use crate::cli::Args;
use crate::utils::errors::R;
use std::fs;
use std::io::Write;
use std::process::exit;

fn main() {
  let args = cli::Args::new(std::env::args());
  match run(&args, &mut std::io::stdout()) {
    Ok(exit_status) => exit(exit_status),
    Err(error) => {
      eprintln!("ERROR: {}", error);
      exit(1);
    }
  };
}

fn run(args: &Args, stdout: &mut dyn Write) -> R<i32> {
  let code = String::from_utf8(fs::read(&args.input)?)?;
  let ast = frontend::run(args.debug, &code)?;
  interpreter::run(args.debug, stdout, ast)
}

#[cfg(test)]
mod file_tests_suite {
  use super::*;
  use std::path::PathBuf;

  enum Expected {
    Success { output: String, status: i32 },
    Errors(String),
  }

  impl Expected {
    fn new(file: &str) -> R<Expected> {
      let success_file = fs::read(PathBuf::from(file).with_extension("success"));
      let expected_errors = fs::read(PathBuf::from(file).with_extension("errors"));
      match (success_file, expected_errors) {
        (Ok(success_file), Err(_)) => Expected::parse_success_file(success_file),
        (Err(_), Ok(expected_errors)) => Ok(Expected::Errors(String::from_utf8(expected_errors)?)),
        (Ok(_), Ok(_)) => Err("both .success and .errors file found".into()),
        (Err(_), Err(_)) => Err("missing .success or .errors file".into()),
      }
    }

    fn parse_success_file(success_file: Vec<u8>) -> R<Expected> {
      let success_file = String::from_utf8(success_file)?;
      let mut lines = success_file.lines();
      let mut result = String::new();
      let mut last = lines
        .next()
        .expect("success file should contain the exitcode");
      for line in lines {
        result.push_str(last);
        result.push('\n');
        last = line;
      }
      Ok(Expected::Success {
        output: result,
        status: last.parse()?,
      })
    }
  }

  mod interpreted {
    use super::*;
    use pretty_assertions::assert_eq;
    use std::io::Cursor;

    fn test_file_interpreted(file: &str) -> R<()> {
      let mut stdout = Cursor::new(vec![]);
      let result = run(
        &Args {
          input: PathBuf::from(file),
          debug: false,
        },
        &mut stdout,
      );
      match Expected::new(file)? {
        Expected::Success {
          output: expected_output,
          status: expected_status,
        } => match result {
          Err(err) => {
            eprintln!("{}", err);
            panic!();
          }
          Ok(result) => {
            assert_eq!(
              String::from_utf8(stdout.get_ref().clone())?,
              expected_output
            );
            assert_eq!(result, expected_status);
          }
        },
        Expected::Errors(expected_errors) => {
          assert_eq!(result.map_err(|x| format!("{}\n", x)), Err(expected_errors));
        }
      }
      Ok(())
    }

    file_tests::generate_file_tests!(test_file_interpreted);
  }
}
