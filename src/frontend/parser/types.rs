use crate::frontend::parser::parse_error;
use crate::frontend::parser::Parser;
use crate::frontend::tokenizer::Token;
use crate::utils::errors::OptionExt;
use crate::utils::fold_left_first;
use crate::R;
use indexmap::IndexMap;
use Token::*;

#[derive(Debug, PartialEq, Clone)]
pub enum Type {
  Identifier(String),
  Function(Box<Type>, Box<Type>),
  Object(IndexMap<String, Type>),
  Union(Box<Type>, Box<Type>),
}

impl<'a> Parser<'a> {
  pub fn type_ascription(&mut self) -> R<Option<Type>> {
    Ok(if self.0.peek().transpose_ref()? == Some(&Token::Colon) {
      self.exact(Token::Colon)?;
      let typ = self.typ()?;
      Some(typ)
    } else {
      None
    })
  }

  fn typ(&mut self) -> R<Type> {
    self.type_arrow_chain()
  }

  fn type_arrow_chain(&mut self) -> R<Type> {
    let mut chain = vec![self.non_arrow_type()?];
    while let Some(Arrow) = self.0.peek().transpose_ref()? {
      self.exact(Arrow)?;
      chain.push(self.non_arrow_type()?);
    }
    Ok(
      fold_left_first(chain.into_iter(), |next, acc| {
        Type::Function(Box::new(next), Box::new(acc))
      })
      .unwrap(),
    )
  }

  fn non_arrow_type(&mut self) -> R<Type> {
    let mut result = self.non_intersection_type()?;
    while let Some(Pipe) = self.0.peek().transpose_ref()? {
      self.exact(Pipe)?;
      result = Type::Union(Box::new(result), Box::new(self.non_intersection_type()?));
    }
    Ok(result)
  }

  fn non_intersection_type(&mut self) -> R<Type> {
    Ok(match self.0.peek().transpose_ref()? {
      Some(Identifier(_)) => Type::Identifier(self.identifier()?),
      Some(RoundBracketOpen) => {
        self.exact(RoundBracketOpen)?;
        let typ = self.typ()?;
        self.exact(RoundBracketClose)?;
        typ
      }
      Some(CurlyBracketOpen) => {
        self.exact(CurlyBracketOpen)?;
        let mut result = IndexMap::new();
        loop {
          if self.0.peek().transpose_ref()? == Some(&CurlyBracketClose) {
            break;
          }
          let field_name = self.identifier()?;
          self.exact(Colon)?;
          let field_type = self.typ()?;
          self.exact(Semicolon)?;
          result.insert(field_name, field_type);
        }
        self.exact(CurlyBracketClose)?;
        Type::Object(result)
      }
      got => parse_error("type", got)?,
    })
  }
}
