use crate::frontend::comments::{strip_comments, StripComments};
use crate::utils::errors::RS;
use peekmore::PeekMore;
use peekmore::PeekMoreIterator;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Token {
  Identifier(String),
  StringLiteral(String),
  RoundBracketOpen,
  RoundBracketClose,
  CurlyBracketOpen,
  CurlyBracketClose,
  Equal,
  Semicolon,
  Dot,
  Lambda,
  Colon,
  Arrow,
  Pipe,
  Ampersand,
}

pub fn run(code: &str) -> Tokenizer {
  Tokenizer::new(code)
}

pub struct Tokenizer<'a> {
  string_tokens: Vec<(Vec<Option<char>>, Token)>,
  input: PeekMoreIterator<StripComments<'a>>,
}

impl<'a> Tokenizer<'a> {
  fn new(code: &str) -> Tokenizer {
    use Token::*;
    let string_tokens = vec![
      ("(", RoundBracketOpen),
      (")", RoundBracketClose),
      ("{", CurlyBracketOpen),
      ("}", CurlyBracketClose),
      ("=", Equal),
      (";", Semicolon),
      (".", Dot),
      ("\\", Lambda),
      (":", Colon),
      ("->", Arrow),
      ("|", Pipe),
      ("&", Ampersand),
    ]
    .into_iter()
    .map(|(string, token)| (string.chars().map(Some).collect(), token))
    .collect();
    Tokenizer {
      string_tokens,
      input: strip_comments(code).peekmore(),
    }
  }

  fn lookup_string_token(&mut self) -> Option<Token> {
    for (key, token) in self.string_tokens.iter() {
      let next = self.input.peek_amount(key.len());
      if next == key {
        for _ in 0..key.len() {
          self.input.next();
        }
        return Some(token.clone());
      }
    }
    None
  }

  fn is_identifier_character(char: char) -> bool {
    matches!(char, 'a'..='z') || char == '_'
  }

  fn identifier(&mut self) -> String {
    let mut result = String::new();
    loop {
      match self.input.peek().cloned() {
        Some(char) if Tokenizer::is_identifier_character(char) => {
          self.input.next();
          result.push(char);
        }
        _ => break,
      }
    }
    result
  }

  fn string_literal(&mut self) -> RS<String> {
    match self.input.next() {
      Some('\"') => {
        let mut result = String::new();
        loop {
          match self.input.next() {
            Some('\"') => break,
            Some(char) => result.push(char),
            None => return Err("unexpected EOF within string literal".to_string()),
          }
        }
        Ok(result)
      }
      Some(unexpected) => Err(format!("unexpected char: {}, expected: \"", unexpected)),
      None => Err("unexpected EOF, expected: \"".to_string()),
    }
  }
}

impl<'a> Iterator for Tokenizer<'a> {
  type Item = RS<Token>;

  fn next(&mut self) -> Option<Self::Item> {
    use Token::*;
    if let Some(token) = self.lookup_string_token() {
      return Some(Ok(token));
    }
    match self.input.peek() {
      Some(' ') | Some('\n') => {
        self.input.next();
        self.next()
      }
      Some('\"') => Some(self.string_literal().map(StringLiteral)),
      Some(char) if Tokenizer::is_identifier_character(*char) => {
        Some(Ok(Identifier(self.identifier())))
      }
      Some(unexpected) => Some(Err(format!(
        "unexpected char: {:?}, expected: valid token",
        unexpected
      ))),
      None => None,
    }
  }
}

#[cfg(test)]
mod test {
  use super::Token::*;
  use super::*;

  fn test(code: &str) -> Vec<Token> {
    run(code).take(100).collect::<RS<Vec<Token>>>().unwrap()
  }

  mod identifiers {
    use super::*;

    #[test]
    fn simple() {
      assert_eq!(test("foo"), vec![Identifier("foo".to_string())]);
    }

    #[test]
    fn with_underscores() {
      assert_eq!(test("foo_bar"), vec![Identifier("foo_bar".to_string())]);
    }
  }

  #[test]
  fn function_applications() {
    assert_eq!(
      test("foo(bar)"),
      vec![
        Identifier("foo".to_string()),
        RoundBracketOpen,
        Identifier("bar".to_string()),
        RoundBracketClose
      ]
    );
  }

  #[test]
  fn string_literals() {
    assert_eq!(test("\"foo\""), vec![StringLiteral("foo".to_string())]);
  }

  #[test]
  fn spaces_between_tokens() {
    assert_eq!(
      test("foo bar"),
      vec![Identifier("foo".to_string()), Identifier("bar".to_string())]
    );
  }

  #[test]
  fn newlines_between_tokens() {
    assert_eq!(
      test("foo\nbar"),
      vec![Identifier("foo".to_string()), Identifier("bar".to_string())]
    );
  }

  #[test]
  fn objects() {
    assert_eq!(
      test("{ foo = bar; }"),
      vec![
        CurlyBracketOpen,
        Identifier("foo".to_string()),
        Equal,
        Identifier("bar".to_string()),
        Semicolon,
        CurlyBracketClose
      ]
    );
  }

  #[test]
  fn empty_object() {
    assert_eq!(test("{ }"), vec![CurlyBracketOpen, CurlyBracketClose]);
    assert_eq!(test("{}"), vec![CurlyBracketOpen, CurlyBracketClose]);
  }

  #[test]
  fn accessors() {
    assert_eq!(
      test("foo.bar"),
      vec![
        Identifier("foo".to_string()),
        Dot,
        Identifier("bar".to_string()),
      ]
    );
  }

  #[test]
  fn lambdas() {
    assert_eq!(
      test("\\ foo . bar"),
      vec![
        Lambda,
        Identifier("foo".to_string()),
        Dot,
        Identifier("bar".to_string()),
      ]
    );
  }

  #[test]
  fn type_ascription() {
    assert_eq!(
      test("foo : bar"),
      vec![
        Identifier("foo".to_string()),
        Colon,
        Identifier("bar".to_string()),
      ]
    );
  }

  #[test]
  fn function_types() {
    assert_eq!(
      test("foo -> bar"),
      vec![
        Identifier("foo".to_string()),
        Arrow,
        Identifier("bar".to_string()),
      ]
    );
  }

  #[test]
  fn union_types() {
    assert_eq!(
      test("foo | bar"),
      vec![
        Identifier("foo".to_string()),
        Pipe,
        Identifier("bar".to_string()),
      ]
    );
  }

  #[test]
  fn intersections() {
    assert_eq!(
      test("foo & bar"),
      vec![
        Identifier("foo".to_string()),
        Ampersand,
        Identifier("bar".to_string()),
      ]
    );
  }
}
