pub mod types;

use super::tokenizer::{Token, Tokenizer};
use crate::frontend::parser::types::Type;
use crate::utils::errors::OptionExt;
use crate::utils::errors::R;
use crate::utils::errors::RS;
use indexmap::IndexMap;
use std::{fmt::Debug, iter::Peekable};
use Ast::*;

#[derive(Debug, PartialEq, Clone)]
pub enum Ast {
  Object(IndexMap<String, Ast>),
  Identifier(String),
  StringLiteral(String),
  Application(Box<Ast>, Box<Ast>),
  Accessor(Box<Ast>, String),
  Lambda(String, Option<Type>, Box<Ast>),
  Typed(Box<Ast>, Type),
  Intersection(Box<Ast>, Box<Ast>),
}

pub fn run(tokens: Tokenizer) -> R<Ast> {
  Parser::new(tokens).parse()
}

struct Parser<'a>(Peekable<Tokenizer<'a>>);

impl<'a> Parser<'a> {
  fn new(tokens: Tokenizer) -> Parser {
    Parser(tokens.peekable())
  }

  fn next(&mut self) -> RS<Option<Token>> {
    self.0.next().transpose()
  }

  fn parse(&mut self) -> R<Ast> {
    let result = self.expression()?;
    self.eof(". or expression")?;
    Ok(result)
  }

  fn expression(&mut self) -> R<Ast> {
    self.intersection_chain()
  }

  fn intersection_chain(&mut self) -> R<Ast> {
    let mut result = self.application_chain()?;
    while let Some(Token::Ampersand) = self.0.peek().transpose_ref()? {
      self.exact(Token::Ampersand)?;
      result = Intersection(Box::new(result), Box::new(self.application_chain()?));
    }
    Ok(result)
  }

  fn application_chain(&mut self) -> R<Ast> {
    let first = self.non_application_expression()?;
    let mut chain = vec![];
    while let Some(next) = self.0.peek().transpose_ref()? {
      if Parser::is_expression_initial_token(next) {
        chain.push(self.non_application_expression()?);
      } else {
        break;
      }
    }
    let result = chain.into_iter().fold(first, |acc, next| {
      Application(Box::new(acc), Box::new(next))
    });
    Ok(match self.type_ascription()? {
      None => result,
      Some(typ) => Typed(Box::new(result), typ),
    })
  }

  fn is_expression_initial_token(token: &Token) -> bool {
    matches!(
      token,
      Token::CurlyBracketOpen
        | Token::Identifier(_)
        | Token::StringLiteral(_)
        | Token::RoundBracketOpen
        | Token::Lambda
    )
  }

  fn non_application_expression(&mut self) -> R<Ast> {
    let mut expression = self.non_accessor_expression()?;
    while let Some(Token::Dot) = self.0.peek().transpose_ref()? {
      self.next()?;
      let accessor = self.identifier()?;
      expression = Accessor(Box::new(expression), accessor);
    }
    Ok(expression)
  }

  fn non_accessor_expression(&mut self) -> R<Ast> {
    fn error<T>(got: Option<&Token>) -> R<T> {
      parse_error("{, (, identifier or string literal", got)
    }

    match self.0.peek().transpose_ref()? {
      Some(token) if Parser::is_expression_initial_token(token) => match token {
        Token::CurlyBracketOpen => self.object(),
        Token::Identifier(_) => self.identifier().map(Identifier),
        Token::StringLiteral(_) => self.string_literal().map(StringLiteral),
        Token::RoundBracketOpen => {
          self.next()?;
          let expression = self.expression()?;
          self.exact(Token::RoundBracketClose)?;
          Ok(expression)
        }
        Token::Lambda => self.lambda(),
        got => error(Some(got)),
      },
      got => error(got),
    }
  }

  fn lambda(&mut self) -> R<Ast> {
    self.exact(Token::Lambda)?;
    let (parameter, typ) = self.parameter()?;
    self.exact(Token::Dot)?;
    let body = self.application_chain()?;
    Ok(Lambda(parameter, typ, Box::new(body)))
  }

  fn parameter(&mut self) -> R<(String, Option<Type>)> {
    Ok(match self.0.peek().transpose_ref()? {
      Some(Token::RoundBracketOpen) => {
        self.exact(Token::RoundBracketOpen)?;
        let result = self.identifier()?;
        let typ = self.type_ascription()?;
        self.exact(Token::RoundBracketClose)?;
        (result, typ)
      }
      Some(Token::Identifier(_)) => (self.identifier()?, self.type_ascription()?),
      got => parse_error("( or identifier", got)?,
    })
  }

  fn object(&mut self) -> R<Ast> {
    use Token::*;
    self.exact(CurlyBracketOpen)?;
    let mut map = IndexMap::new();
    loop {
      if !matches!(self.0.peek().transpose_ref()?, Some(Token::Identifier(_))) {
        break;
      }
      let key = self.identifier()?;
      self.exact(Equal)?;
      let value = self.expression()?;
      self.exact(Semicolon)?;
      map.insert(key, value);
    }
    self.exact(CurlyBracketClose)?;
    Ok(Object(map))
  }

  fn exact(&mut self, expected: Token) -> R<()> {
    match self.next()? {
      Some(token) if token == expected => Ok(()),
      got => parse_error(expected, got),
    }
  }

  fn identifier(&mut self) -> R<String> {
    match self.next()? {
      Some(Token::Identifier(string)) => Ok(string),
      got => parse_error("identifier", got),
    }
  }

  fn string_literal(&mut self) -> R<String> {
    match self.next()? {
      Some(Token::StringLiteral(string)) => Ok(string),
      got => parse_error("string literal", got),
    }
  }

  fn eof(&mut self, expected: &str) -> R<()> {
    match self.next()? {
      None => Ok(()),
      Some(next) => Err(format!("expected: {}, got: {:?}", expected, next).into()),
    }
  }
}

fn parse_error<Expected: Debug, Got: Debug, T>(expected: Expected, got: Option<Got>) -> R<T> {
  match got {
    Some(got) => Err(format!("expected: {:?}, got: {:?}", expected, got).into()),
    None => Err(format!("expected: {:?}, got: EOF", expected).into()),
  }
}

#[cfg(test)]
pub mod test {
  use super::*;
  use crate::frontend::tokenizer;
  use crate::utils::errors::ResultExt;
  use crate::utils::errors::RS;
  use pretty_assertions::assert_eq;

  fn test(code: &str, ast: RS<Ast>) {
    assert_eq!(run(tokenizer::run(code)).into_rs(), ast);
  }

  #[test]
  fn hello_world() {
    let mut expected = IndexMap::new();
    expected.insert(
      "main".to_string(),
      Application(
        Box::new(Identifier("println".to_string())),
        Box::new(StringLiteral("foo".to_string())),
      ),
    );
    test("{ main = println(\"foo\"); }", Ok(Object(expected)));
  }

  #[test]
  fn allows_white_space_between_tokens() {
    let mut map = IndexMap::new();
    map.insert("main".to_string(), Identifier("foo".to_string()));
    test("  {   main \n = foo ; \n}\n ", Ok(Object(map)));
  }

  mod objects {
    use super::*;

    #[test]
    fn empty_object() {
      test("{}", Ok(Object(IndexMap::new())));
    }

    #[test]
    fn objects_with_multiple_fields() {
      let mut expected = IndexMap::new();
      expected.insert("foo".to_string(), Identifier("bar".to_string()));
      expected.insert("baz".to_string(), Identifier("huhu".to_string()));
      test("{ foo = bar; baz = huhu; }", Ok(Object(expected)));
    }

    #[test]
    fn object_in_brackets() {
      test("({})", Ok(Object(IndexMap::new())));
    }
  }

  #[test]
  fn identifiers() {
    test("foo", Ok(Identifier("foo".to_string())));
  }

  #[test]
  fn string_literals() {
    test("\"foo\"", Ok(StringLiteral("foo".to_string())));
  }

  #[test]
  fn bracketed_expression() {
    test("(foo)", Ok(Identifier("foo".to_string())));
  }

  #[test]
  fn errors_on_trailing_tokens() {
    test(
      "foo)",
      Err("expected: . or expression, got: RoundBracketClose".to_string()),
    );
  }

  mod applications {
    use super::*;

    #[test]
    fn function_applications() {
      test(
        "foo bar",
        Ok(Application(
          Box::new(Identifier("foo".to_string())),
          Box::new(Identifier("bar".to_string())),
        )),
      );
    }

    #[test]
    fn application_with_brackets() {
      test(
        "foo(bar)",
        Ok(Application(
          Box::new(Identifier("foo".to_string())),
          Box::new(Identifier("bar".to_string())),
        )),
      );
    }

    #[test]
    fn application_chains() {
      test(
        "foo bar baz",
        Ok(Application(
          Box::new(Application(
            Box::new(Identifier("foo".to_string())),
            Box::new(Identifier("bar".to_string())),
          )),
          Box::new(Identifier("baz".to_string())),
        )),
      );
    }

    #[test]
    fn left_bracketing() {
      test(
        "(foo bar) baz",
        Ok(Application(
          Box::new(Application(
            Box::new(Identifier("foo".to_string())),
            Box::new(Identifier("bar".to_string())),
          )),
          Box::new(Identifier("baz".to_string())),
        )),
      );
    }

    #[test]
    fn right_bracketing() {
      test(
        "foo (bar baz)",
        Ok(Application(
          Box::new(Identifier("foo".to_string())),
          Box::new(Application(
            Box::new(Identifier("bar".to_string())),
            Box::new(Identifier("baz".to_string())),
          )),
        )),
      );
    }
  }

  mod accessors {
    use super::*;

    #[test]
    fn one_accessor() {
      test(
        "foo.bar",
        Ok(Accessor(
          Box::new(Identifier("foo".to_string())),
          "bar".to_string(),
        )),
      );
    }

    #[test]
    fn multiple_accessors() {
      test(
        "foo.bar.baz",
        Ok(Accessor(
          Box::new(Accessor(
            Box::new(Identifier("foo".to_string())),
            "bar".to_string(),
          )),
          "baz".to_string(),
        )),
      );
    }
  }

  mod lambdas {
    use super::*;

    #[test]
    fn simple() {
      test(
        "\\ foo . bar",
        Ok(Lambda(
          "foo".to_string(),
          None,
          Box::new(Identifier("bar".to_string())),
        )),
      );
    }

    #[test]
    fn with_application() {
      test(
        "\\ foo . bar baz",
        Ok(Lambda(
          "foo".to_string(),
          None,
          Box::new(Application(
            Box::new(Identifier("bar".to_string())),
            Box::new(Identifier("baz".to_string())),
          )),
        )),
      );
    }

    #[test]
    fn with_accessor() {
      test(
        "\\ foo . bar.baz",
        Ok(Lambda(
          "foo".to_string(),
          None,
          Box::new(Accessor(
            Box::new(Identifier("bar".to_string())),
            "baz".to_string(),
          )),
        )),
      );
    }

    #[test]
    fn round_bracketed() {
      test(
        "(\\ foo . bar) baz",
        Ok(Application(
          Box::new(Lambda(
            "foo".to_string(),
            None,
            Box::new(Identifier("bar".to_string())),
          )),
          Box::new(Identifier("baz".to_string())),
        )),
      );
    }

    #[test]
    fn with_round_bracketed_parameter() {
      test(
        "\\ (foo) . bar",
        Ok(Lambda(
          "foo".to_string(),
          None,
          Box::new(Identifier("bar".to_string())),
        )),
      );
    }

    #[test]
    fn bracketed_typed_parameter() {
      test(
        "\\ (foo : bar) . baz",
        Ok(Lambda(
          "foo".to_string(),
          Some(Type::Identifier("bar".to_string())),
          Box::new(Identifier("baz".to_string())),
        )),
      );
    }

    #[test]
    fn unbracketed_typed_parameter() {
      test(
        "\\ foo : bar . baz",
        Ok(Lambda(
          "foo".to_string(),
          Some(Type::Identifier("bar".to_string())),
          Box::new(Identifier("baz".to_string())),
        )),
      );
    }
  }

  mod type_ascription {
    use super::*;

    #[test]
    fn simple() {
      test(
        "a : b",
        Ok(Typed(
          Box::new(Identifier("a".to_string())),
          Type::Identifier("b".to_string()),
        )),
      );
    }

    #[test]
    fn function_types() {
      test(
        "a : b -> c",
        Ok(Typed(
          Box::new(Identifier("a".to_string())),
          Type::Function(
            Box::new(Type::Identifier("b".to_string())),
            Box::new(Type::Identifier("c".to_string())),
          ),
        )),
      );
    }

    #[test]
    fn function_types_are_right_associative() {
      test(
        "a : b -> c -> d",
        Ok(Typed(
          Box::new(Identifier("a".to_string())),
          Type::Function(
            Box::new(Type::Identifier("b".to_string())),
            Box::new(Type::Function(
              Box::new(Type::Identifier("c".to_string())),
              Box::new(Type::Identifier("d".to_string())),
            )),
          ),
        )),
      );
    }

    #[test]
    fn good_error_messages_when_parsing_types() {
      test(
        "a : ()",
        Err("expected: \"type\", got: RoundBracketClose".to_string()),
      );
    }

    #[test]
    fn functions_can_be_left_bracketed() {
      test(
        "a : (b -> c) -> d",
        Ok(Typed(
          Box::new(Identifier("a".to_string())),
          Type::Function(
            Box::new(Type::Function(
              Box::new(Type::Identifier("b".to_string())),
              Box::new(Type::Identifier("c".to_string())),
            )),
            Box::new(Type::Identifier("d".to_string())),
          ),
        )),
      );
    }

    #[test]
    fn intersection_types() {
      test(
        "a : foo | bar",
        Ok(Typed(
          Box::new(Identifier("a".to_string())),
          Type::Union(
            Box::new(Type::Identifier("foo".to_string())),
            Box::new(Type::Identifier("bar".to_string())),
          ),
        )),
      );
    }

    #[test]
    fn multiple_intersections() {
      test(
        "a : foo | bar | baz",
        Ok(Typed(
          Box::new(Identifier("a".to_string())),
          Type::Union(
            Box::new(Type::Union(
              Box::new(Type::Identifier("foo".to_string())),
              Box::new(Type::Identifier("bar".to_string())),
            )),
            Box::new(Type::Identifier("baz".to_string())),
          ),
        )),
      );
    }

    #[test]
    fn intersection_return_type_binds_closer_than_arrow() {
      test(
        "a : foo -> bar | baz",
        Ok(Typed(
          Box::new(Identifier("a".to_string())),
          Type::Function(
            Box::new(Type::Identifier("foo".to_string())),
            Box::new(Type::Union(
              Box::new(Type::Identifier("bar".to_string())),
              Box::new(Type::Identifier("baz".to_string())),
            )),
          ),
        )),
      );
    }

    #[test]
    fn intersection_parameter_type_binds_closer_than_arrow() {
      test(
        "a : foo | bar -> baz",
        Ok(Typed(
          Box::new(Identifier("a".to_string())),
          Type::Function(
            Box::new(Type::Union(
              Box::new(Type::Identifier("foo".to_string())),
              Box::new(Type::Identifier("bar".to_string())),
            )),
            Box::new(Type::Identifier("baz".to_string())),
          ),
        )),
      );
    }
  }

  mod function_intersection {
    use super::*;

    #[test]
    fn simple_union() {
      test(
        "a & b",
        Ok(Intersection(
          Box::new(Identifier("a".to_string())),
          Box::new(Identifier("b".to_string())),
        )),
      );
    }

    #[test]
    fn lambdas() {
      test(
        r#"\ a . b & \ c . d"#,
        Ok(Intersection(
          Box::new(Lambda(
            "a".to_string(),
            None,
            Box::new(Identifier("b".to_string())),
          )),
          Box::new(Lambda(
            "c".to_string(),
            None,
            Box::new(Identifier("d".to_string())),
          )),
        )),
      );
    }

    #[test]
    fn applications() {
      test(
        r#"a b & c d"#,
        Ok(Intersection(
          Box::new(Application(
            Box::new(Identifier("a".to_string())),
            Box::new(Identifier("b".to_string())),
          )),
          Box::new(Application(
            Box::new(Identifier("c".to_string())),
            Box::new(Identifier("d".to_string())),
          )),
        )),
      );
    }

    #[test]
    fn accessors() {
      test(
        r#"a.b & c.d"#,
        Ok(Intersection(
          Box::new(Accessor(
            Box::new(Identifier("a".to_string())),
            "b".to_string(),
          )),
          Box::new(Accessor(
            Box::new(Identifier("c".to_string())),
            "d".to_string(),
          )),
        )),
      );
    }

    #[test]
    fn round_brackets() {
      test(
        r#" a & (b & c) "#,
        Ok(Intersection(
          Box::new(Identifier("a".to_string())),
          Box::new(Intersection(
            Box::new(Identifier("b".to_string())),
            Box::new(Identifier("c".to_string())),
          )),
        )),
      );
      test(
        r#" (a & b) & c "#,
        Ok(Intersection(
          Box::new(Intersection(
            Box::new(Identifier("a".to_string())),
            Box::new(Identifier("b".to_string())),
          )),
          Box::new(Identifier("c".to_string())),
        )),
      );
    }

    #[test]
    fn type_ascriptions() {
      test(
        r#"\ a . b & \ c . d : foo"#,
        Ok(Intersection(
          Box::new(Lambda(
            "a".to_string(),
            None,
            Box::new(Identifier("b".to_string())),
          )),
          Box::new(Lambda(
            "c".to_string(),
            None,
            Box::new(Typed(
              Box::new(Identifier("d".to_string())),
              Type::Identifier("foo".to_string()),
            )),
          )),
        )),
      );
    }
  }

  #[cfg(feature = "proptests")]
  pub mod proptests {
    use super::*;
    use crate::frontend::parser;
    use proptest::collection::vec;
    use proptest::option::weighted;
    use proptest::result::Probability;
    use proptest::{prelude::*, sample::select};

    fn identifier(limit: i32) -> impl Strategy<Value = String> {
      let identifiers: Vec<String> = ('a'..'z')
        .take(limit as usize)
        .map(|char| char.to_string())
        .collect();
      prop_oneof![
        4 => select(identifiers),
        1 => select(vec!["println".to_string(), "concat".to_string()]),
      ]
    }

    pub fn parsed() -> impl Strategy<Value = parser::Ast> {
      (1..10).prop_flat_map(|limit| {
        let leafs = prop_oneof![
          identifier(limit).prop_map(Identifier),
          select(vec!["foo", "bar"]).prop_map(|s| StringLiteral(s.to_string()))
        ];
        leafs.prop_recursive(8, 256, 10, move |inner| {
          prop_oneof![
            (inner.clone(), inner.clone()).prop_map(|(a, b)| Application(Box::new(a), Box::new(b))),
            (
              identifier(limit),
              weighted(Probability::new(0.2), type_strategy(limit)),
              inner.clone()
            )
              .prop_map(|(parameter, parameter_type, body)| Lambda(
                parameter,
                parameter_type,
                Box::new(body)
              )),
            vec((identifier(limit), inner.clone()), 0..5)
              .prop_map(|fields| Object(fields.into_iter().collect())),
            (inner.clone(), identifier(limit))
              .prop_map(|(expression, accessor)| Accessor(Box::new(expression), accessor)),
            (inner.clone(), inner.clone())
              .prop_map(|(a, b)| Intersection(Box::new(a), Box::new(b))),
            (inner, type_strategy(limit))
              .prop_map(|(expression, typ)| Typed(Box::new(expression), typ)),
          ]
        })
      })
    }

    fn type_strategy(limit: i32) -> impl Strategy<Value = Type> {
      let leaf = identifier(limit).prop_map(Type::Identifier);
      leaf.prop_recursive(4, 10, 10, move |inner| {
        prop_oneof![
          (inner.clone(), inner.clone())
            .prop_map(|(a, b)| Type::Function(Box::new(a), Box::new(b))),
          vec((identifier(limit), inner.clone()), 0..5)
            .prop_map(|fields| Type::Object(fields.into_iter().collect())),
          (inner.clone(), inner).prop_map(|(a, b)| Type::Union(Box::new(a), Box::new(b))),
        ]
      })
    }
  }
}
