mod dependency_analysis;
mod external_type;
mod inference;
pub mod internal_type;
#[cfg(test)]
mod test;

use crate::frontend::name_resolution;
use crate::utils::pretty::Pretty;
use crate::R;
pub use external_type::Type;
use inference::type_term;
pub use inference::Ast;

pub fn run(debug: bool, ast: name_resolution::Ast) -> R<Ast<Type>> {
  let ast = type_term(ast)?;
  if debug {
    eprintln!("INTERNAL TYPE:\n{}", ast.typ.pretty());
  }
  let ast = external_type::to_external_ast(ast)?;
  if debug {
    eprintln!("TYPE:\n{}", ast.typ.pretty());
  }
  Ok(ast)
}
