use super::*;
use guard::guard_unwrap;
use InternalType::*;

pub mod deep_clone {
  use super::*;

  #[test]
  fn clones_internal_types() {
    assert_eq!(
      InternalType::String.deep_clone(&mut HashMap::new()),
      InternalType::String
    );
  }

  #[test]
  fn clones_variables_into_new_distinct_variables() {
    let variable_state = VariableState::fresh_var(0);
    let clone = Variable(variable_state.clone()).deep_clone(&mut HashMap::new());
    guard_unwrap!(let Variable(variable_state_clone) = clone);
    assert_ne!(variable_state_clone, variable_state);
    assert_ne!(
      variable_state_clone.lower_bounds,
      variable_state.lower_bounds
    );
    assert_ne!(
      variable_state_clone.upper_bounds,
      variable_state.upper_bounds
    );
  }

  #[test]
  fn clones_level_and_bounds() {
    let variable_state = VariableState::fresh_var(42);
    variable_state
      .lower_bounds
      .access(|lower_bounds| lower_bounds.push(String));
    variable_state
      .upper_bounds
      .access(|upper_bounds| upper_bounds.push(Object(Map::new())));
    let variable = Variable(variable_state.clone());
    let clone = variable.deep_clone(&mut HashMap::new());
    guard_unwrap!(let Variable(variable_state_clone) = clone);
    assert_eq!(variable_state_clone.level, 42);
    assert_eq!(
      variable_state_clone.lower_bounds.access(|x| x.clone()),
      variable_state.lower_bounds.access(|x| x.clone()),
    );
    assert_eq!(
      variable_state_clone.upper_bounds.access(|x| x.clone()),
      variable_state.upper_bounds.access(|x| x.clone()),
    );
  }

  #[test]
  fn maps_identical_variables_to_the_same_variables() {
    let variable_state = VariableState::fresh_var(0);
    let function = Function(
      Box::new(Variable(variable_state.clone())),
      Box::new(Variable(variable_state)),
    );
    let clone = function.deep_clone(&mut HashMap::new());
    guard_unwrap!(let Function(a, b) = &clone);
    assert_eq!(a, b);
    assert_ne!(clone, function);
  }

  #[test]
  fn allows_to_map_identical_variables_across_multiple_calls() {
    let variable = Variable(VariableState::fresh_var(0));
    let variable_map = &mut HashMap::new();
    let clone_a = variable.deep_clone(variable_map);
    let clone_b = variable.deep_clone(variable_map);
    assert_eq!(clone_a, clone_b);
  }
}

#[cfg(feature = "proptests")]
pub mod proptests {
  use super::*;
  use proptest::{collection::vec, prelude::*};

  impl InternalType {
    fn visit<F>(&self, seen: &mut HashSet<VariableState>, f: &mut F)
    where
      F: FnMut(&InternalType),
    {
      use InternalType::*;
      match self {
        String => {}
        Function(a, b) => {
          a.visit(seen, f);
          b.visit(seen, f);
        }
        Union(a, b) => {
          a.visit(seen, f);
          b.visit(seen, f);
        }
        Object(map) => {
          for (_, field) in map.iter() {
            field.visit(seen, f);
          }
        }
        Variable(variable) => {
          if seen.contains(variable) {
            return;
          } else {
            seen.insert(variable.clone());
            for lower_bound in variable.lower_bounds.iter_cloned() {
              lower_bound.visit(seen, f);
            }
            for upper_bound in variable.upper_bounds.iter_cloned() {
              upper_bound.visit(seen, f);
            }
          }
        }
      }
      f(self);
    }

    fn collect_variables(&self) -> HashSet<VariableState> {
      let mut result = HashSet::new();
      self.visit(&mut HashSet::new(), &mut |typ| {
        if let InternalType::Variable(variable) = typ {
          result.insert(variable.clone());
        }
      });
      result
    }
  }

  pub fn internal_type() -> BoxedStrategy<InternalType> {
    (
      vec(internal_type_without_bounds(), 1..5),
      vec(((0 as usize)..20, (0 as usize)..10), 0..20),
      vec(((0 as usize)..20, (0 as usize)..10), 0..20),
    )
      .prop_map(
        |(types, lower_bound_assignments, upper_bound_assignments)| {
          let variables: Vec<VariableState> = types
            .iter()
            .map(|typ| typ.collect_variables())
            .flatten()
            .collect();
          for (variable_index, type_index) in lower_bound_assignments.iter() {
            if let (Some(variable), Some(typ)) =
              (variables.get(*variable_index), types.get(*type_index))
            {
              variable.lower_bounds.access(|x| x.push(typ.clone()));
            }
          }
          for (variable_index, type_index) in upper_bound_assignments.iter() {
            if let (Some(variable), Some(typ)) =
              (variables.get(*variable_index), types.get(*type_index))
            {
              variable.upper_bounds.access(|x| x.push(typ.clone()));
            }
          }
          types.get(0).unwrap().clone()
        },
      )
      .boxed()
  }

  fn internal_type_without_bounds() -> BoxedStrategy<InternalType> {
    let leaf = prop_oneof![
      Just(InternalType::String),
      ((0 as i64)..10).prop_map(|level| InternalType::Variable(VariableState::fresh_var(level)))
    ];
    leaf
      .prop_recursive(8, 20, 5, move |inner| {
        prop_oneof![
          (inner.clone(), inner.clone())
            .prop_map(|(a, b)| InternalType::Function(Box::new(a), Box::new(b))),
          (inner.clone(), inner.clone())
            .prop_map(|(a, b)| InternalType::Union(Box::new(a), Box::new(b))),
          vec(("[a-z]{1,3}", inner), 0..10)
            .prop_map(|fields| InternalType::Object(fields.into_iter().collect())),
        ]
      })
      .boxed()
  }

  proptest! {
    #[test]
    fn doesnt_crash(typ in internal_type()) {
      typ.deep_clone(&mut HashMap::new());
    }

    #[test]
    fn deep_cloned_types_have_the_same_string_representation(typ in internal_type()) {
      let result = typ.deep_clone(&mut HashMap::new()).pretty();
      let expected = typ.pretty();
      assert_eq!(result, expected);
    }

    #[test]
    fn clone_does_not_share_variables_with_input(typ in internal_type()) {
      let clone = typ.deep_clone(&mut HashMap::new());
      let empty: Vec<&VariableState> = vec![];
      assert_eq!(
        typ.collect_variables().intersection(&clone.collect_variables()).collect::<Vec<&VariableState>>(),
        empty
      );
    }
  }
}
