use crate::{
  frontend::name_resolution::{Ast, Id},
  utils::map::Map,
};
use indexmap::IndexMap;
use petgraph::algo::kosaraju_scc;
use petgraph::Directed;
use petgraph::Graph;
use std::collections::HashMap;
use std::hash::Hash;

pub type Groups = Vec<Vec<Id>>;

pub fn group(map: &IndexMap<Id, Ast>) -> Groups {
  let graph = to_graph(map);
  let sccs = kosaraju_scc(&graph);
  let mut result = vec![];
  for component in sccs {
    let mut group = Vec::new();
    for node_index in component.into_iter() {
      if let Some(key) = graph.node_weight(node_index) {
        group.push(key.clone());
      }
    }
    result.push(group);
  }
  result
}

fn to_graph(map: &IndexMap<Id, Ast>) -> Graph<Id, (), Directed> {
  let mut result = Graph::new();
  let mut node_indices = HashMap::new();
  for key in map.keys().rev() {
    let node_index = result.add_node(key.clone());
    node_indices.insert(key, node_index);
  }
  for node_index in result.node_indices() {
    let id = result.node_weight(node_index).unwrap();
    let ast = map.get(id).unwrap();
    for used in ast.collect_identifiers() {
      if let Some(target) = node_indices.get(&used) {
        result.add_edge(node_index, *target, ());
      }
    }
  }
  result
}

pub fn sort_same_as<K, A>(mut a: Map<K, A>, b: &mut dyn Iterator<Item = &K>) -> Map<K, A>
where
  K: Eq + Hash,
{
  let mut result = Map::new();
  for key in b {
    if let Some((key, value)) = a.remove_entry(key) {
      result.insert(key, value);
    }
  }
  for (key, value) in a.into_iter() {
    result.insert(key, value);
  }
  result
}

#[cfg(test)]
mod test {
  use super::*;

  mod group {
    use super::*;
    use crate::builtins::BuiltinName;
    use crate::frontend::name_resolution::{AstNode, Id};
    use pretty_assertions::assert_eq;
    use std::iter::once;

    fn test(groups: Groups, expected: Vec<Vec<&str>>) {
      assert_eq!(
        groups
          .into_iter()
          .map(|group| group.into_iter().map(|id| id.name).collect::<Vec<_>>())
          .collect::<Vec<_>>(),
        expected
      );
    }

    #[test]
    fn can_group_empty_objects() {
      test(group(&IndexMap::new()), vec![]);
    }

    #[test]
    fn returns_a_given_field() {
      let object: IndexMap<Id, Ast> = once((
        Id {
          id: 0,
          name: "foo".to_string(),
        },
        Ast(AstNode::Builtin(BuiltinName::Println)),
      ))
      .collect();
      test(group(&object), vec![vec!["foo"]]);
    }

    #[test]
    fn returns_unrelated_fields_in_separate_groups() {
      let a = (
        Id {
          id: 0,
          name: "a".to_string(),
        },
        Ast(AstNode::Builtin(BuiltinName::Println)),
      );
      let b = (
        Id {
          id: 1,
          name: "b".to_string(),
        },
        Ast(AstNode::Builtin(BuiltinName::Println)),
      );
      let object: IndexMap<Id, Ast> = vec![a, b].into_iter().collect();
      test(group(&object), vec![vec!["a"], vec!["b"]]);
    }

    #[test]
    fn sorts_fields_topologically() {
      let a = (
        Id {
          id: 0,
          name: "a".to_string(),
        },
        Ast(AstNode::Identifier(Id {
          id: 1,
          name: "b".to_string(),
        })),
      );
      let b = (
        Id {
          id: 1,
          name: "b".to_string(),
        },
        Ast(AstNode::Builtin(BuiltinName::Println)),
      );
      let object: IndexMap<Id, Ast> = vec![a, b].into_iter().collect();
      test(group(&object), vec![vec!["b"], vec!["a"]]);
    }

    #[test]
    fn groups_mutually_recursive_definitions() {
      let a = (
        Id {
          id: 0,
          name: "a".to_string(),
        },
        Ast(AstNode::Identifier(Id {
          name: "b".to_string(),
          id: 1,
        })),
      );
      let b = (
        Id {
          id: 1,
          name: "b".to_string(),
        },
        Ast(AstNode::Identifier(Id {
          name: "a".to_string(),
          id: 0,
        })),
      );
      let object: IndexMap<Id, Ast> = vec![a, b].into_iter().collect();
      test(group(&object), vec![vec!["b", "a"]]);
    }

    #[test]
    fn puts_simple_recursive_definitions_into_one_group() {
      let a = (
        Id {
          id: 0,
          name: "a".to_string(),
        },
        Ast(AstNode::Identifier(Id {
          name: "a".to_string(),
          id: 0,
        })),
      );
      let b = (
        Id {
          id: 1,
          name: "b".to_string(),
        },
        Ast(AstNode::Builtin(BuiltinName::Println)),
      );
      let object: IndexMap<Id, Ast> = vec![a, b].into_iter().collect();
      test(group(&object), vec![vec!["a"], vec!["b"]]);
    }

    #[test]
    fn works_when_free_variables_are_used() {
      let a = (
        Id {
          id: 0,
          name: "a".to_string(),
        },
        Ast(AstNode::Identifier(Id {
          name: "free".to_string(),
          id: 1,
        })),
      );
      let object: IndexMap<Id, Ast> = vec![a].into_iter().collect();
      test(group(&object), vec![vec!["a"]]);
    }

    #[test]
    fn uses_ids_for_identifier_equality() {
      let id = (
        Id {
          id: 0,
          name: "id".to_string(),
        },
        Ast(AstNode::Lambda {
          parameter: Id {
            name: "a".to_string(),
            id: 1,
          },
          parameter_type: None,
          body: Box::new(Ast(AstNode::Identifier(Id {
            name: "a".to_string(),
            id: 1,
          }))),
        }),
      );
      let a = (
        Id {
          id: 2,
          name: "a".to_string(),
        },
        Ast(AstNode::Identifier(Id {
          id: 0,
          name: "id".to_string(),
        })),
      );
      let object: IndexMap<Id, Ast> = vec![id, a].into_iter().collect();
      test(group(&object), vec![vec!["id"], vec!["a"]]);
    }
  }

  mod sort_same_as {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn sorts_one_map_in_the_same_order_as_another() {
      let a: Map<i64, i64> = vec![(2, 2), (1, 1), (3, 3)].into_iter().collect();
      let b: Vec<i64> = vec![(1), (2), (3)].into_iter().collect();
      let expected: Map<i64, i64> = vec![(1, 1), (2, 2), (3, 3)].into_iter().collect();
      assert_eq!(sort_same_as(a, &mut b.iter()), expected);
    }

    #[test]
    fn adds_elements_that_are_missing_from_the_second_map() {
      let a: Map<i64, i64> = vec![(1, 1)].into_iter().collect();
      let b: Vec<i64> = Vec::new();
      assert_eq!(sort_same_as(a.clone(), &mut b.iter()), a);
    }
  }
}
