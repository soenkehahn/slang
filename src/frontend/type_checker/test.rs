use super::*;
use crate::{frontend::parser, utils::errors::ResultExt};
use crate::{frontend::tokenizer, utils::pretty::Pretty};

fn test(code: &str) -> R<String> {
  let ast = run(
    true,
    name_resolution::run(parser::run(tokenizer::run(code))?)?,
  )?;
  Ok(ast.typ.pretty())
}

macro_rules! test_table {
    (($test_name:ident, $code:literal, $expected:expr),) => {
      #[test]
      fn $test_name() {
        pretty_assertions::assert_eq!(
           test($code).into_rs(),
          $expected.map(|x: &str| x.to_string()).map_err(|error: &str| error.to_string())
        );
      }
    };
    (($test_name:ident, $code:literal, $expected:expr), $(($a:ident, $b:literal, $c:expr),)+) => {
      test_table![($test_name, $code, $expected),];
      test_table![$(($a, $b, $c),)+];
    };
  }

test_table![
  (string_literal, r#" "foo" "#, Ok("string")),
  (identity_function_1, r#" (\ x . x) "foo" "#, Ok("string")),
  (identity_function_2, r#" (\ x . x) {} "#, Ok("{}")),
  (const_function, r#" (\ x . "foo") {} "#, Ok("string")),
  (nested_functions, r#" (\ x . (\ y . y) x) {} "#, Ok("{}")),
  (
    higher_order_function,
    r#" (\ id . id "foo") (\ x . x) "#,
    Ok("string")
  ),
  (
    object_application,
    r#" {} "foo" "#,
    Err("cannot constrain {} <: (string -> <bottom>)")
  ),
  (println, r#" println "foo" "#, Ok("{}")),
  (
    println_2,
    r#" println {} "#,
    Err("cannot constrain {} <: string")
  ),
  (objects, r#"{}"#, Ok("{}")),
  (objects_2, r#" { foo = "foo"; } "#, Ok("{ foo: string; }")),
  (
    nested_objects,
    r#" { foo = { bar = "bar"; }; } "#,
    Ok("{ foo: { bar: string; }; }")
  ),
  (
    objects_with_multiple_fields,
    r#" { foo = "foo"; bar = {}; } "#,
    Ok("{ foo: string; bar: {}; }")
  ),
  (
    objects_with_unknowns,
    r#" { foo = \ x . x; } "#,
    Ok("{ foo: (a -> a); }")
  ),
  (accessors, r#" { foo = "foo"; }.foo "#, Ok("string")),
  (
    accessor_error,
    r#" { foo = "foo"; }.bar "#,
    Err("cannot constrain { foo: string; } <: { bar: <bottom>; }\n(missing field: bar)")
  ),
  (
    accessor_error_2,
    r#" "foo".bar "#,
    Err("cannot constrain string <: { bar: <bottom>; }")
  ),
  (
    infinite_recursion,
    r#" { foo = bar; bar = foo; } "#,
    Ok("{ foo: <bottom>; bar: <bottom>; }")
  ),
  (
    accessor_inference,
    r#" (\ x . x.foo) { foo = "foo"; } "#,
    Ok("string")
  ),
  (
    accessor_inference_2,
    r#" (\ x . x.f x.a) { f = \ x . x; a = "foo"; } "#,
    Ok("string")
  ),
  (
    type_ascription,
    "\\ x . (x : string)",
    Ok("(string -> string)")
  ),
  (
    type_ascription_function_types,
    r#"(\ x . x) : string -> string"#,
    Ok("(string -> string)")
  ),
  (
    type_ascription_lambdas,
    r#"\ x . x : string"#,
    Ok("(string -> string)")
  ),
  (type_ascription_empty_object_type, r#"{} : {}"#, Ok("{}")),
  (
    type_ascription_object_types,
    r#"{ foo = "foo"; } : { foo: string; }"#,
    Ok("{ foo: string; }")
  ),
  (
    type_ascription_object_types_2,
    r#"{ foo = "foo"; bar = {}; } : { foo: string; bar: {}; }"#,
    Ok("{ foo: string; bar: {}; }")
  ),
  (
    type_ascription_error,
    r#"(\ x . x) : string -> {}"#,
    Err("cannot constrain string <: {}")
  ),
  (
    type_ascription_curried_functions,
    r#"(\ a . \ b . a) : string -> string -> string"#,
    Ok("(string -> (string -> string))")
  ),
  (
    type_ascription_higher_order_functions,
    r#"(\ f . \ x . f x) : (string -> string) -> string -> string"#,
    Ok("((string -> string) -> (string -> string))")
  ),
  (
    parameter_types,
    r#" \ (x: string) . x "#,
    Ok("(string -> string)")
  ),
  (
    unknown_nested_parameter,
    r#" \ a: string . \ b . a "#,
    Ok("(string -> (<top> -> string))")
  ),
  (
    polymorphism_1,
    r#"
      {
        foo = \ x . "";
      }
    "#,
    Ok("{ foo: (<top> -> string); }")
  ),
  (
    polymorphism_2,
    r#"{
      id = \ x . x;
      foo = {
        string = id "foo";
        object = id {};
      };
    }.foo"#,
    Ok("{ string: string; object: {}; }")
  ),
  (
    polymorphism_3,
    r#"
      {
        a = id "";
        b = id {};
        id = \ x . x;
      }
    "#,
    Ok("{ a: string; b: {}; id: (c -> c); }")
  ),
  (
    weird_test_1,
    r#"
      (\ o . o.f o.a)
      {
        f = (\ x . "foo") : string -> string;
        a = "foo": string;
      }
    "#,
    Ok("string")
  ),
  (
    weird_test_2,
    r#"
      {
        o = {
          f = \ x . x;
          x = "foo";
        };
        a = o.f o.x;
      }.a
    "#,
    Ok("string")
  ),
  (
    weird_test_3,
    r#"
      {
        id = \ x . x;
        o = {
          a = id "";
          b = id {};
        };
      }.o.a
    "#,
    Ok("string")
  ),
  (
    weird_test_4,
    r#"{ foo = \ x . foo; }"#,
    Ok("{\n  foo: (<top> -> (μ b . (<top> -> b)));\n}")
  ),
  (
    weird_test_5,
    r#"{ foo = \ x . foo; bar = foo; }"#,
    Ok("{\n  foo: (<top> -> (μ b . (<top> -> b)));\n  bar: (<top> -> (μ e . (<top> -> e)));\n}")
  ),
];
