use super::*;
use crate::frontend::name_resolution;
use crate::frontend::type_checker::inference::type_term;
use crate::frontend::type_checker::run;
use crate::utils::errors::ResultExt;
use crate::utils::errors::RS;
use trim_margin::MarginTrimmable;

mod coalesce_type {
  use super::*;
  use crate::frontend::{parser, tokenizer};
  use pretty_assertions::assert_eq;

  fn test(code: &str) -> RS<String> {
    Ok(
      type_term(name_resolution::run(parser::run(tokenizer::run(code)).into_rs()?).into_rs()?)
        .into_rs()?
        .typ
        .coalesce_type()
        .pretty(),
    )
  }

  #[test]
  fn string() {
    assert_eq!(test(r#""foo""#), Ok("string".to_string()));
  }

  #[test]
  fn identity_function() {
    assert_eq!(test(r#"\ x . x"#), Ok("(a -> a)".to_string()));
  }

  #[test]
  fn empty_objects() {
    assert_eq!(test(r#"(\ x . x) {}"#), Ok("(a | {})".to_string()));
  }

  #[test]
  fn object_fields() {
    assert_eq!(
      test(r#"{ foo = "foo"; }"#),
      Ok("{ foo: string; }".to_string())
    );
  }

  #[test]
  fn parameter_subtyping() {
    assert_eq!(
      test(r#" (\ x . x.foo) { foo = "foo"; bar = {}; }"#),
      Ok("(a | string)".to_string())
    );
  }

  #[test]
  fn accessors_on_parameters() {
    assert_eq!(
      test(r#" \ x . x.foo "#),
      Ok("((b & { foo: a; }) -> a)".to_string())
    );
  }

  #[test]
  fn infinite_parameter_type() {
    assert_eq!(
      test(r#" \ x . x x "#),
      Ok("((a & (a -> b)) -> b)".to_string())
    );
  }
}

mod simplify {
  use super::*;
  use crate::frontend::{parser, tokenizer};
  use pretty_assertions::assert_eq;

  fn test(code: &str) -> RS<String> {
    Ok(
      run(
        true,
        name_resolution::run(parser::run(tokenizer::run(code)).into_rs()?).into_rs()?,
      )
      .into_rs()?
      .typ
      .pretty(),
    )
  }

  #[test]
  fn removes_variables_that_are_only_negative() {
    assert_eq!(
      test(r#" \ x . x : string "#),
      Ok("(string -> string)".to_string())
    );
  }

  #[test]
  fn removes_variables_that_are_only_positive() {
    assert_eq!(
      test(r#" \ x . println x "#),
      Ok("(string -> {})".to_string())
    );
  }

  #[test]
  fn does_not_remove_variables_that_are_both_positive_and_negative() {
    assert_eq!(test(r#" \ x . x "#), Ok("(a -> a)".to_string()));
  }

  #[test]
  fn infinite_types() {
    assert_eq!(
      test(r#" { bottom = \ x . bottom; } "#),
      Ok("{\n  bottom: (<top> -> (μ b . (<top> -> b)));\n}".to_string())
    );
  }

  #[test]
  fn simplifies_identical_types() {
    assert_eq!(
      test(
        r#"
          {
            f = \ x . x : string;
            g = \ x . concat x (f x);
          }
        "#
      ),
      Ok("{\n  f: (string -> string);\n  g: (string -> string);\n}".to_string())
    );
  }

  #[test]
  fn weird() {
    assert_eq!(
      test(
        r#"
          {
            f = \ x . x;
            g = \ x . concat x (f x);
          }
        "#
      ),
      Ok("{\n  f: (a -> a);\n  g: (string -> string);\n}".to_string())
    );
  }

  #[test]
  fn sandwiched_variables_are_removed() {
    assert_eq!(
      test(
        r#"
          {
            f = \ x . x : string;
            g = \ x . concat x (f x);
            h = f (g "");
          }
        "#
      ),
      Ok("{\n  f: (string -> string);\n  g: (string -> string);\n  h: string;\n}".to_string())
    );
  }

  mod polymorphism {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn polymorphism() {
      assert_eq!(
        test(
          r#"
            {
              id = \ a . a;
              a = id "";
              b = id {};
            }
          "#,
        ),
        Ok("{ id: (a -> a); a: string; b: {}; }".to_string())
      );
    }

    #[test]
    fn polymorphism_level_violation() {
      assert_eq!(
        test(
          r#"
            \ id . {
              foo = id "";
            }
          "#
        ),
        Ok("((string -> b) -> { foo: b; })".to_string())
      );
    }

    #[test]
    fn self_recursive_fields() {
      assert_eq!(
        test(
          r#"
            {
              foo = foo;
            }
          "#
        ),
        Ok("{ foo: <bottom>; }".to_string())
      );
    }

    #[test]
    fn using_polymorphism_in_nested_object() {
      assert_eq!(
        test(
          r#"
            {
              id = \ x . x;
              object = {
                a = id "";
                b = id {};
              };
            }.object
          "#
        ),
        Ok("{ a: string; b: {}; }".to_string())
      );
    }

    #[test]
    fn bottom_bug() {
      assert_eq!(
        test(
          r#"
            {
              a = \ x . x {};
              b = a {};
            }
          "#
        ),
        Err("cannot constrain {} <: ({} -> <bottom>)".to_string())
      );
    }

    #[test]
    fn mvar_bug_1() {
      assert_eq!(
        test(
          r#"
            {
              bottom = bottom;
              main = bottom;
            }
          "#
        ),
        Ok("{ bottom: <bottom>; main: <bottom>; }".to_string())
      );
    }

    #[test]
    fn mvar_bug_2() {
      assert_eq!(
        test(
          r#"
            {
              lib = {
                id = \ x . id;
              };
            }
          "#
        ),
        Ok(
          "
          |{
          |  lib: {
          |    id: (<top> -> (μ b . (<top> -> b)));
          |  };
          |}
          "
          .trim_margin()
          .unwrap()
        )
      );
    }
  }

  #[test]
  fn y_combinator() {
    assert_eq!(
      test(r#" \ f . (\ x . f (x x)) (\ x . f (x x)) "#),
      Ok("(((a -> b) & (d -> (d & a))) -> b)".to_string())
    );
  }

  #[test]
  fn use_recursive_type_twice() {
    assert_eq!(
      test(r#" (\ f . (\ x . f (x x)) (\ x . f (x x))) (\ rec . { foo = rec; bar = rec; }) "#),
      Ok(
        "
        |{
        |  foo: (μ a . { foo: a; bar: a; });
        |  bar: (μ a . { foo: a; bar: a; });
        |}
        "
        .trim_margin()
        .unwrap()
      )
    );
  }

  #[test]
  fn infinite_return_value() {
    assert_eq!(
      test(r#" (\ f . (\ x . f (x x)) (\ x . f (x x))) (\ rec . { foo = rec; }) "#),
      Ok("{ foo: (μ a . { foo: a; }); }".to_string())
    );
  }

  mod union_types_and_function_intersections {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn union_type_ascription() {
      assert_eq!(
        test(
          r#"
            "foo": string | {}
          "#
        ),
        Ok("(string | {})".to_string())
      );
    }

    #[test]
    fn function_intersection() {
      assert_eq!(
        test(
          r#"
            \ x: string . "" &
            \ x: {} . ""
          "#
        ),
        Ok("((string | {}) -> string)".to_string())
      );
    }

    #[test]
    fn function_intersection_application() {
      assert_eq!(
        test(
          r#"
            {
              f =
                \ x: string . "" &
                \ x: {} . "";
              a = f "";
              b = f {};
            }
          "#
        ),
        Ok(
          "
          |{
          |  f: ((string | {}) -> string);
          |  a: string;
          |  b: string;
          |}
          "
          .trim_margin()
          .unwrap()
        )
      );
    }

    #[test]
    fn intersection_functions_with_union_return_type() {
      assert_eq!(
        test(
          r#"
            \ x: string . {} &
            \ x: {} . ""
          "#
        ),
        Ok("((string | {}) -> ({} | string))".to_string())
      );
    }

    #[test]
    fn union_type_ascription_error() {
      assert_eq!(
        test(r#" "" : {} | {} "#),
        Err("cannot constrain string <: {}".to_string())
      );
    }

    #[test]
    fn merging_of_errors_on_constraining_on_union_types() {
      assert_eq!(
        test(r#" (\ x . x) : string | {} "#),
        Err("cannot constrain (a -> a) <: (string | {})".to_string())
      );
    }

    #[test]
    fn application_on_union_typed_value() {
      assert_eq!(
        test(
          r#"
            {
              f =
                \ x: string . "string" &
                \ x: {} . "object";
              main = f ("" : string | {});
            }
          "#
        ),
        Ok(
          "
          |{
          |  f: ((string | {}) -> string);
          |  main: string;
          |}
          "
          .trim_margin()
          .unwrap()
        )
      );
    }

    #[test]
    fn application_on_union_typed_value_error() {
      assert_eq!(
        test(
          r#"
            {
              f =
                \ x: string . "string" &
                \ x: { a: {}; } . "has a";
              main = f ("" : string | {});
            }
          "#
        ),
        Err(
          "
          |cannot constrain {} <: (string | { a: {}; })
          |(missing field: a)
          "
          .trim_margin()
          .unwrap()
        )
      );
    }

    #[test]
    fn union_subtype_checking_1() {
      assert_eq!(
        test(
          r#"
            {
              union = "" : string | {};
              a = union : {};
            }
          "#
        ),
        Err("cannot constrain string <: {}".to_string())
      );
    }

    #[test]
    fn union_subtype_checking_2() {
      assert_eq!(
        test(
          r#"
            {
              union = "" : string | {};
              a = union : string;
            }
          "#
        ),
        Err("cannot constrain {} <: string".to_string())
      );
    }

    #[test]
    fn deep_clone_non_termination_bug() {
      assert_eq!(
        test(
          r#"
            {
              traverse_foo =
                \ x . traverse_foo x.foo &
                \ x . x;
              id =
                \ x . { foo = id x.foo;} &
                \ x . x;
              x = traverse_foo (id {});
            }
          "#
        ),
        Err("cannot use type variable as parameter type to function intersection".to_string())
      );
    }

    mod undispatchable_types {
      use super::*;
      use pretty_assertions::assert_eq;
      use Type::*;

      #[test]
      fn dispatch_on_functions() {
        assert_eq!(
          test(
            r#"
              \ x: (string -> string) . "string identity" &
              \ x: ({} -> {}) . "object identity"
            "#,
          ),
          Err(
            "cannot use (string -> string) as parameter type to function intersection".to_string()
          )
        );
      }

      #[test]
      fn dispatch_on_type_variables() {
        assert_eq!(
          test(
            r#"
              \ x . x &
              \ x: string . "string"
            "#,
          ),
          Err("cannot use type variable as parameter type to function intersection".to_string())
        );
      }

      #[test]
      fn undispatchable_input_type_in_rhs_of_intersection() {
        assert_eq!(
          test(
            r#"
              \ x: string . "string" &
              \ x . x
            "#,
          ),
          Err("cannot use type variable as parameter type to function intersection".to_string())
        );
      }

      #[test]
      fn dispatch_on_type_variables_can_be_circumvented_by_type_annotations() {
        assert_eq!(
          test(
            r#"
              \ x: {} . x &
              \ x: string . "string"
            "#,
          ),
          Ok("(({} | string) -> ({} | string))".to_string())
        );
      }

      #[test]
      fn rejects_union_types_with_undispatchable_lefts() {
        assert_eq!(
          Union(Box::new(Variable("a".to_string())), Box::new(String))
            .assert_dispatchable_input_type()
            .into_rs(),
          Err("cannot use type variable as parameter type to function intersection".to_string())
        );
      }

      #[test]
      fn rejects_union_types_with_undispatchable_rights() {
        assert_eq!(
          Union(Box::new(String), Box::new(Variable("a".to_string())),)
            .assert_dispatchable_input_type()
            .into_rs(),
          Err("cannot use type variable as parameter type to function intersection".to_string())
        );
      }

      #[test]
      fn rejects_intersection_types_with_undispatchable_lefts() {
        assert_eq!(
          Intersection(Box::new(Variable("a".to_string())), Box::new(String))
            .assert_dispatchable_input_type()
            .into_rs(),
          Err("cannot use type variable as parameter type to function intersection".to_string())
        );
      }

      #[test]
      fn rejects_intersection_types_with_undispatchable_rights() {
        assert_eq!(
          Intersection(Box::new(String), Box::new(Variable("a".to_string())),)
            .assert_dispatchable_input_type()
            .into_rs(),
          Err("cannot use type variable as parameter type to function intersection".to_string())
        );
      }

      #[test]
      fn rejects_recursive_types_with_undispatchable_inner_types() {
        assert_eq!(
          Recursive(
            "foo".to_string(),
            Box::new(Union(
              Box::new(Variable("a".to_string())),
              Box::new(RecursiveVariable("foo".to_string())),
            )),
          )
          .assert_dispatchable_input_type()
          .into_rs(),
          Err("cannot use type variable as parameter type to function intersection".to_string())
        );
      }

      #[test]
      fn accepts_recursive_types_without_undispatchable_inner_types() {
        assert_eq!(
          Recursive(
            "foo".to_string(),
            Box::new(Union(
              Box::new(String),
              Box::new(RecursiveVariable("foo".to_string())),
            )),
          )
          .assert_dispatchable_input_type()
          .into_rs(),
          Ok(())
        );
      }

      #[test]
      fn rejects_objects_with_undispatchable_fields() {
        assert_eq!(
          Object(
            vec![
              ("dispatchable".to_string(), String),
              (
                "undispatchable".to_string(),
                Function(Box::new(String), Box::new(String))
              ),
            ]
            .into_iter()
            .collect(),
          )
          .assert_dispatchable_input_type()
          .into_rs(),
          Err(
            "cannot use (string -> string) as parameter type to function intersection".to_string()
          )
        );
      }

      #[test]
      fn accepts_objects_with_dispatchable_fields() {
        assert_eq!(
          Object(
            vec![("dispatchable".to_string(), String),]
              .into_iter()
              .collect(),
          )
          .assert_dispatchable_input_type()
          .into_rs(),
          Ok(())
        );
      }

      #[test]
      fn top_is_dispatchable() {
        assert_eq!(Top.assert_dispatchable_input_type().into_rs(), Ok(()));
      }

      #[test]
      fn bottom_is_dispatchable() {
        assert_eq!(Bottom.assert_dispatchable_input_type().into_rs(), Ok(()));
      }

      #[test]
      fn rejects_bottom_as_intersection_element() {
        assert_eq!(
          test(r#" \ a . (a & a) "#),
          Err("cannot use value of type <bottom> in intersection".into())
        );
      }
    }
  }
}
