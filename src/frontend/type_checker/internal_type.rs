#[cfg(test)]
pub mod test;

use crate::{
  frontend::name_resolution,
  utils::{
    map::Map,
    mvar::MVar,
    pretty::{render_object, Pretty},
  },
};
use std::collections::HashMap;
use std::collections::HashSet;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Polarity {
  Positive,
  Negative,
}

impl Polarity {
  pub fn swap(self) -> Polarity {
    match self {
      Polarity::Positive => Polarity::Negative,
      Polarity::Negative => Polarity::Positive,
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct VariableState {
  level: i64,
  pub lower_bounds: MVar<Vec<InternalType>>,
  pub upper_bounds: MVar<Vec<InternalType>>,
}

impl VariableState {
  pub fn fresh_var(level: i64) -> VariableState {
    VariableState {
      level,
      lower_bounds: MVar::new(vec![]),
      upper_bounds: MVar::new(vec![]),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum InternalType {
  String,
  Function(Box<InternalType>, Box<InternalType>),
  Variable(VariableState),
  Object(Map<String, InternalType>),
  Union(Box<InternalType>, Box<InternalType>),
}

impl Pretty for InternalType {
  fn pretty(&self) -> String {
    match self {
      InternalType::String => "string".to_string(),
      InternalType::Function(a, b) => {
        format!("({} -> {})", a.pretty(), b.pretty())
      }
      InternalType::Variable(variable_state) => {
        let lower_bounds = match variable_state.lower_bounds.pretty().as_str() {
          "[]" => "".to_string(),
          s => format!("{} <: ", s),
        };
        let upper_bounds = match variable_state.upper_bounds.pretty().as_str() {
          "[]" => "".to_string(),
          s => format!(" <: {}", s),
        };
        format!(
          "{}_({}){}",
          lower_bounds, variable_state.level, upper_bounds
        )
      }
      InternalType::Object(map) => render_object(":", map.iter()),
      InternalType::Union(a, b) => format!("({} | {})", a.pretty(), b.pretty()),
    }
  }
}

impl InternalType {
  pub fn new(typ: &name_resolution::Type) -> InternalType {
    match typ {
      name_resolution::Type::String => InternalType::String,
      name_resolution::Type::Object(map) => {
        let mut result = Map::new();
        for (key, value) in map {
          result.insert(key.clone(), InternalType::new(value));
        }
        InternalType::Object(result)
      }
      name_resolution::Type::Function(a, b) => InternalType::Function(
        Box::new(InternalType::new(a)),
        Box::new(InternalType::new(b)),
      ),
      name_resolution::Type::Union(a, b) => InternalType::Union(
        Box::new(InternalType::new(a)),
        Box::new(InternalType::new(b)),
      ),
    }
  }

  pub fn level(&self) -> i64 {
    match self {
      InternalType::String => 0,
      InternalType::Object(map) => map.iter().map(|(_, x)| x.level()).max().unwrap_or(0),
      InternalType::Variable(variable_state) => variable_state.level,
      InternalType::Function(a, b) => a.level().max(b.level()),
      InternalType::Union(a, b) => a.level().max(b.level()),
    }
  }

  pub fn freshen_above(&self, limit: i64, new_level: i64) -> InternalType {
    self.freshen_above_helper(limit, new_level, &mut HashMap::new())
  }

  fn freshen_above_helper(
    &self,
    limit: i64,
    new_level: i64,
    fresh_variables: &mut HashMap<VariableState, VariableState>,
  ) -> InternalType {
    if self.level() <= limit {
      return self.clone();
    }
    match self {
      InternalType::String => InternalType::String,
      InternalType::Function(a, b) => InternalType::Function(
        Box::new(a.freshen_above_helper(limit, new_level, fresh_variables)),
        Box::new(b.freshen_above_helper(limit, new_level, fresh_variables)),
      ),
      InternalType::Union(a, b) => InternalType::Union(
        Box::new(a.freshen_above_helper(limit, new_level, fresh_variables)),
        Box::new(b.freshen_above_helper(limit, new_level, fresh_variables)),
      ),
      InternalType::Object(map) => InternalType::Object(
        map
          .iter()
          .map(|(key, typ)| {
            (
              key.clone(),
              typ.freshen_above_helper(limit, new_level, fresh_variables),
            )
          })
          .collect(),
      ),
      InternalType::Variable(variable_state) => {
        if variable_state.level <= limit {
          self.clone()
        } else {
          match fresh_variables.get(variable_state) {
            Some(new) => InternalType::Variable(new.clone()),
            None => {
              let new_variable_state = VariableState::fresh_var(new_level);
              fresh_variables.insert(variable_state.clone(), new_variable_state.clone());
              let new_lower_bounds = variable_state
                .lower_bounds
                .iter_cloned()
                .map(|bound| bound.freshen_above_helper(limit, new_level, fresh_variables))
                .collect::<Vec<_>>();
              new_variable_state.lower_bounds.access(|lower_bounds| {
                *lower_bounds = new_lower_bounds;
              });
              let new_upper_bounds = variable_state
                .upper_bounds
                .iter_cloned()
                .map(|bound| bound.freshen_above_helper(limit, new_level, fresh_variables))
                .collect::<Vec<_>>();
              new_variable_state.upper_bounds.access(|upper_bounds| {
                *upper_bounds = new_upper_bounds;
              });
              InternalType::Variable(new_variable_state)
            }
          }
        }
      }
    }
  }

  pub fn deep_clone(
    &self,
    fresh_variables: &mut HashMap<VariableState, VariableState>,
  ) -> InternalType {
    let mut variable_queue = vec![];
    let result = self.deep_clone_without_bounds(fresh_variables, &mut variable_queue);
    InternalType::deep_clone_add_bounds(fresh_variables, variable_queue);
    result
  }

  fn deep_clone_add_bounds(
    fresh_variables: &mut HashMap<VariableState, VariableState>,
    mut variable_queue: Vec<(VariableState, VariableState)>,
  ) {
    let mut seen_variables = HashSet::new();
    while let Some((old, new)) = variable_queue.pop() {
      if !seen_variables.contains(&old) {
        for lower_bound in old.lower_bounds.iter_cloned() {
          let new_lower_bound =
            lower_bound.deep_clone_without_bounds(fresh_variables, &mut variable_queue);
          new.lower_bounds.access(|x| x.push(new_lower_bound));
        }
        for upper_bound in old.upper_bounds.iter_cloned() {
          let new_upper_bound =
            upper_bound.deep_clone_without_bounds(fresh_variables, &mut variable_queue);
          new.upper_bounds.access(|x| x.push(new_upper_bound));
        }
        seen_variables.insert(old.clone());
      }
    }
  }

  fn deep_clone_without_bounds(
    &self,
    fresh_variables: &mut HashMap<VariableState, VariableState>,
    variable_queue: &mut Vec<(VariableState, VariableState)>,
  ) -> InternalType {
    use InternalType::*;
    match self {
      String => String,
      Function(a, b) => Function(
        Box::new(a.deep_clone_without_bounds(fresh_variables, variable_queue)),
        Box::new(b.deep_clone_without_bounds(fresh_variables, variable_queue)),
      ),
      Variable(old) => match fresh_variables.get(old) {
        Some(new) => Variable(new.clone()),
        None => {
          let new = VariableState::fresh_var(old.level);
          fresh_variables.insert(old.clone(), new.clone());
          variable_queue.push((old.clone(), new.clone()));
          Variable(new)
        }
      },
      Object(map) => Object(
        map
          .iter()
          .map(|(key, value)| {
            (
              key.clone(),
              value.deep_clone_without_bounds(fresh_variables, variable_queue),
            )
          })
          .collect(),
      ),
      Union(a, b) => Union(
        Box::new(a.deep_clone_without_bounds(fresh_variables, variable_queue)),
        Box::new(b.deep_clone_without_bounds(fresh_variables, variable_queue)),
      ),
    }
  }

  pub fn extrude(&self, polarity: Polarity, level: i64) -> InternalType {
    self.extrude_helper(polarity, level, &mut HashMap::new())
  }

  fn extrude_helper(
    &self,
    polarity: Polarity,
    new_level: i64,
    fresh_variables: &mut HashMap<VariableState, VariableState>,
  ) -> InternalType {
    if self.level() <= new_level {
      self.clone()
    } else {
      match self {
        InternalType::String => InternalType::String,
        InternalType::Function(a, b) => InternalType::Function(
          Box::new(a.extrude_helper(polarity.swap(), new_level, fresh_variables)),
          Box::new(b.extrude_helper(polarity, new_level, fresh_variables)),
        ),
        InternalType::Object(map) => InternalType::Object(
          map
            .iter()
            .map(|(key, field)| {
              (
                key.clone(),
                field.extrude_helper(polarity, new_level, fresh_variables),
              )
            })
            .collect(),
        ),
        InternalType::Union(a, b) => InternalType::Union(
          Box::new(a.extrude_helper(polarity, new_level, fresh_variables)),
          Box::new(b.extrude_helper(polarity, new_level, fresh_variables)),
        ),
        InternalType::Variable(variable_state) => match fresh_variables.get(variable_state) {
          Some(cached) => InternalType::Variable(cached.clone()),
          None => {
            let new_variable_state = VariableState::fresh_var(new_level);
            fresh_variables.insert(variable_state.clone(), new_variable_state.clone());
            match polarity {
              Polarity::Positive => {
                variable_state.upper_bounds.access(|upper_bounds| {
                  upper_bounds.push(InternalType::Variable(new_variable_state.clone()))
                });
                let new_lower_bounds = variable_state
                  .lower_bounds
                  .iter_cloned()
                  .map(|lower_bound| {
                    lower_bound.extrude_helper(polarity, new_level, fresh_variables)
                  })
                  .collect();
                new_variable_state.lower_bounds.access(|lower_bounds| {
                  *lower_bounds = new_lower_bounds;
                });
              }
              Polarity::Negative => {
                variable_state.lower_bounds.access(|lower_bounds| {
                  lower_bounds.push(InternalType::Variable(new_variable_state.clone()))
                });
                let new_upper_bounds = variable_state
                  .upper_bounds
                  .iter_cloned()
                  .map(|upper_bound| {
                    upper_bound.extrude_helper(polarity, new_level, fresh_variables)
                  })
                  .collect();
                new_variable_state.upper_bounds.access(|upper_bounds| {
                  *upper_bounds = new_upper_bounds;
                });
              }
            }
            InternalType::Variable(new_variable_state)
          }
        },
      }
    }
  }
}
