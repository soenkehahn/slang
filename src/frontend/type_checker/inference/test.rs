use super::*;
use crate::{frontend::parser, utils::errors::RS};
use crate::{
  frontend::{name_resolution, tokenizer},
  utils::errors::ResultExt,
};
use pretty_assertions::assert_eq;
use std::iter::once;
use trim_margin::MarginTrimmable;

fn test(code: &str) -> RS<String> {
  Ok(
    type_term(name_resolution::run(parser::run(tokenizer::run(code)).into_rs()?).into_rs()?)
      .into_rs()?
      .typ
      .pretty(),
  )
}

#[test]
fn string_literals() {
  assert_eq!(test(r#" "foo" "#).unwrap(), "string");
}

#[test]
fn looks_up_identifiers_in_the_context() {
  let context = once((
    Id {
      id: 0,
      name: "foo".to_string(),
    },
    TypeScheme::MonomorphicType(InternalType::String),
  ))
  .collect();
  assert_eq!(
    type_term_helper(
      name_resolution::Ast(AstNode::Identifier(Id {
        id: 0,
        name: "foo".to_string()
      })),
      &context,
      0,
      &mut HashSet::new(),
    )
    .unwrap()
    .typ
    .pretty(),
    "string",
  );
}

#[test]
fn identity_function() {
  assert_eq!(test(r#"\ x . x"#).unwrap(), "(_(0) -> _(0))");
}

#[test]
fn to_string() {
  assert_eq!(test(r#"\ x . "foo""#).unwrap(), "(_(0) -> string)");
}

#[test]
fn paramater_type() {
  assert_eq!(test(r#"\ x: string . x"#).unwrap(), "(string -> string)");
}

#[test]
fn identity_application() {
  assert_eq!(
    test(r#"(\ x . x) "foo""#),
    Ok("[string] <: _(0)".to_string())
  );
}

#[test]
fn empty_objects() {
  assert_eq!(test(r#"{}"#), Ok("{}".to_string()));
}

#[test]
fn higher_order_function() {
  assert_eq!(
    test(r#" (\ id . id "foo") (\ x . x) "#),
    Ok("[string] <: _(0)".to_string())
  );
}

#[test]
fn println() {
  assert_eq!(test(r#"println"#), Ok("(string -> {})".to_string()));
}

#[test]
fn object_fields() {
  assert_eq!(
    test(r#"{ foo = "foo"; }"#),
    Ok("{ foo: string; }".to_string())
  );
}

#[test]
fn type_ascription() {
  assert_eq!(test(r#" "foo" : string "#), Ok("string".to_string()));
}

#[test]
fn type_ascription_error() {
  assert_eq!(
    test(r#" "foo" : {} "#),
    Err("cannot constrain string <: {}".to_string())
  );
}

#[test]
fn type_ascription_with_objects() {
  assert_eq!(
    test(r#"{ foo = "foo"; } : { foo: string; }"#),
    Ok("{ foo: string; }".to_string())
  );
}

#[test]
fn type_ascription_gets_preserved_in_the_ast() {
  let code = r#" "foo" : string "#;
  let ast =
    type_term(name_resolution::run(parser::run(tokenizer::run(code)).unwrap()).unwrap()).unwrap();
  assert_eq!(
    ast,
    Ast {
      ast: AstNode::Typed(
        Box::new(Ast {
          ast: AstNode::StringLiteral("foo".to_string()),
          typ: InternalType::String
        }),
        name_resolution::Type::String
      ),
      typ: InternalType::String
    }
  );
}

#[test]
fn inner_expression_type_can_be_different_from_ascribed_type() {
  let code = r#" "foo" : string | {} "#;
  let ast =
    type_term(name_resolution::run(parser::run(tokenizer::run(code)).unwrap()).unwrap()).unwrap();
  assert_eq!(
    ast,
    Ast {
      ast: AstNode::Typed(
        Box::new(Ast {
          ast: AstNode::StringLiteral("foo".to_string()),
          typ: InternalType::String
        }),
        name_resolution::Type::Union(
          Box::new(name_resolution::Type::String),
          Box::new(name_resolution::Type::Object(IndexMap::new()))
        )
      ),
      typ: InternalType::Union(
        Box::new(InternalType::String),
        Box::new(InternalType::Object(Map::new()))
      )
    }
  );
}

#[test]
fn object_subtyping() {
  assert_eq!(
    test(r#"{ foo = "foo"; bar = {}; } : { foo: string; }"#),
    Ok("{ foo: string; }".to_string())
  );
}

#[test]
fn accessor() {
  assert_eq!(
    test(r#"{ foo = "foo"; }.foo"#),
    Ok("[string] <: _(0)".to_string())
  );
}

#[test]
fn accessor_error() {
  assert_eq!(
    test(r#"{ foo = "foo"; }.bar"#),
    Err(
      "
      |cannot constrain { foo: string; } <: { bar: <bottom>; }
      |(missing field: bar)
      "
      .trim_margin()
      .unwrap()
    )
  );
}

#[test]
fn subtyping_for_parameters() {
  assert_eq!(
    test(r#" (\ x . x.foo) { foo = "foo"; bar = {}; }"#),
    Ok("[string] <: _(0)".to_string())
  );
}

mod siblings {
  use super::*;
  use pretty_assertions::assert_eq;

  #[test]
  fn using_sibling_fields() {
    assert_eq!(
      test(r#" { a = "foo"; b = a; }"#),
      Ok("{ a: string; b: string; }".to_string())
    );
  }

  #[test]
  fn using_later_sibling_fields() {
    assert_eq!(
      test(r#" { a = b; b = "foo"; }"#),
      Ok("{ a: string; b: string; }".to_string())
    );
  }

  #[test]
  fn sibling_error() {
    assert_eq!(
      test(r#" { a = {}; b = println a; }"#),
      Err("cannot constrain {} <: string".to_string())
    );
  }

  #[test]
  fn sibling_subtyping_1() {
    assert_eq!(
      test(r#" { a = { foo = ""; bar = {}; }; b = println a.foo; }.b"#),
      Ok("[{}] <: _(0)".to_string())
    );
  }

  #[test]
  fn sibling_subtyping_2() {
    assert_eq!(
      test(r#" { a = { bar = {}; }; b = println a.foo; }.b"#),
      Err(
        "
        |cannot constrain { bar: {}; } <: { foo: <bottom>; }
        |(missing field: foo)
        "
        .trim_margin()
        .unwrap()
      )
    );
  }
}

#[test]
fn can_print_out_cyclical_bounds() {
  assert!(test(
    r#"
        {
          id = \ a . a;
          const = (\ a . \ b . a);
          test = (\ x . (((const const) id) x)) {};
        }
      "#,
  )
  .unwrap()
  .contains("const"));
}

#[test]
fn variable_that_does_not_need_freshening() {
  assert_eq!(
    test(
      r#"
        \ x . {
          foo = x;
          bar = foo;
        }
      "#
    ),
    Ok(
      "
        |(_(0) <: [_(0), _(0)] -> {
        |  foo: _(0) <: [_(0), _(0)];
        |  bar: _(0) <: [_(0), _(0)];
        |})
      "
      .trim_margin()
      .unwrap()
    )
  );
}

#[test]
fn infinite_types() {
  assert_eq!(
    test(r#" { bottom = \ x . bottom; } "#),
    Ok("{\n  bottom: (_(1) -> [(_(1) -> <locked> <: _(1))] <: _(1));\n}".to_string())
  );
}

mod union_types_and_function_intersections {
  use super::*;
  use pretty_assertions::assert_eq;

  #[test]
  fn union_type_ascription() {
    assert_eq!(
      test(
        r#"
          "foo" : string | {}
        "#
      ),
      Ok("(string | {})".to_string())
    );
  }

  #[test]
  fn function_intersection() {
    assert_eq!(
      test(
        r#"
          \ x: string . "" &
          \ x: {} . ""
        "#
      ),
      Ok("((_(0) <: [string] | _(0) <: [{}]) -> [string] <: _(0))".to_string())
    );
  }

  #[test]
  fn function_intersection_application() {
    assert_eq!(
      test(
        r#"
          {
            f =
              \ x: string . "" &
              \ x: {} . "";
            main = f "";
          }
        "#
      ),
      Ok(
        "
        |{
        |  f: ((_(2) <: [string] | _(2) <: [{}]) -> [string] <: _(2));
        |  main: [string] <: _(1) <: [[string] <: _(1)];
        |}
        "
        .trim_margin()
        .unwrap()
      )
    );
  }

  #[test]
  fn type_error_for_unions_of_non_functions() {
    assert_eq!(
      test(r#" "" & {} "#),
      Err("intersections are only supported for functions".to_string())
    );
    assert_eq!(
      test(r#" \ x . x & {} "#),
      Err("intersections are only supported for functions".to_string())
    );
  }
}

#[cfg(feature = "proptests")]
mod proptests {
  use super::*;
  use crate::frontend::type_checker::internal_type::test::proptests::internal_type;
  use proptest::prelude::*;

  proptest! {
    #[test]
    fn constrain_does_not_crash((a, b) in (internal_type(), internal_type())) {
      let _ = constrain(&a, &b, &mut HashSet::new());
    }
  }
}
