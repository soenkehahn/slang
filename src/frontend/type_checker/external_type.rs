#[cfg(test)]
mod test;

use super::internal_type::{InternalType, Polarity, VariableState};
use crate::frontend::{Ast, AstNode};
use crate::utils::{
  errors::R,
  pretty::{render_object, Pretty},
  unique_names::UniqueNames,
};
use indexmap::IndexMap;
use std::collections::{HashMap, HashSet};

pub fn to_external_ast(ast: Ast<InternalType>) -> R<Ast<Type>> {
  let ast = ast.map(&mut |simple_typ: InternalType| simple_typ.coalesce_type().simplify());
  ast.assert_dispatchability()?;
  Ok(ast)
}

impl InternalType {
  pub fn coalesce_type(&self) -> Type {
    self.coalesce_type_helper(
      Polarity::Positive,
      &HashSet::new(),
      &mut HashMap::new(),
      &mut UniqueNames::new(),
    )
  }

  fn coalesce_type_helper(
    &self,
    polarity: Polarity,
    in_process: &HashSet<(VariableState, Polarity)>,
    recursive_variables: &mut HashMap<(VariableState, Polarity), String>,
    unique_names: &mut UniqueNames<VariableState>,
  ) -> Type {
    match self {
      InternalType::String => Type::String,
      InternalType::Function(f, x) => Type::Function(
        Box::new(f.coalesce_type_helper(
          polarity.swap(),
          in_process,
          recursive_variables,
          unique_names,
        )),
        Box::new(x.coalesce_type_helper(polarity, in_process, recursive_variables, unique_names)),
      ),
      InternalType::Object(map) => {
        let mut result = IndexMap::new();
        for (key, value) in map.iter() {
          result.insert(
            key.clone(),
            value.coalesce_type_helper(polarity, in_process, recursive_variables, unique_names),
          );
        }
        Type::Object(result)
      }
      InternalType::Union(a, b) => Type::Union(
        Box::new(a.coalesce_type_helper(polarity, in_process, recursive_variables, unique_names)),
        Box::new(b.coalesce_type_helper(polarity, in_process, recursive_variables, unique_names)),
      ),
      InternalType::Variable(variable_state) => {
        let variable_state_polarity = (variable_state.clone(), polarity);
        if in_process.contains(&variable_state_polarity) {
          match recursive_variables.get(&variable_state_polarity) {
            Some(name) => Type::RecursiveVariable(name.clone()),
            None => {
              let new_name = unique_names.new_name();
              recursive_variables.insert(variable_state_polarity, new_name.clone());
              Type::RecursiveVariable(new_name)
            }
          }
        } else {
          let in_process = {
            let mut new = in_process.clone();
            new.insert(variable_state_polarity.clone());
            new
          };
          type MergeFunction = fn(Box<Type>, Box<Type>) -> Type;
          let (merge, bounds): (MergeFunction, _) = match polarity {
            Polarity::Positive => (Type::Union, variable_state.lower_bounds.clone()),
            Polarity::Negative => (Type::Intersection, variable_state.upper_bounds.clone()),
          };
          #[allow(clippy::needless_collect)]
          let bound_types: Vec<_> = bounds
            .iter_cloned()
            .map(|bound| {
              bound.coalesce_type_helper(polarity, &in_process, recursive_variables, unique_names)
            })
            .collect();
          let result: Type = bound_types.into_iter().fold(
            Type::Variable(unique_names.get_name(variable_state.clone())),
            |acc, next| merge(Box::new(acc), Box::new(next)),
          );
          match recursive_variables.get(&variable_state_polarity) {
            None => result,
            Some(recursive_name) => Type::Recursive(recursive_name.clone(), Box::new(result)),
          }
        }
      }
    }
  }
}

#[derive(Debug, PartialEq, Clone)]
pub enum Type {
  String,
  Function(Box<Type>, Box<Type>),
  Object(IndexMap<String, Type>),
  Union(Box<Type>, Box<Type>),
  Bottom,
  Intersection(Box<Type>, Box<Type>),
  Top,
  Variable(String),
  Recursive(String, Box<Type>),
  RecursiveVariable(String),
}

impl Pretty for Type {
  fn pretty(&self) -> String {
    match self {
      Type::String => "string".to_string(),
      Type::Function(f, x) => format!("({} -> {})", f.pretty(), x.pretty()),
      Type::Union(a, b) => format!("({} | {})", a.pretty(), b.pretty()),
      Type::Bottom => "<bottom>".to_string(),
      Type::Intersection(a, b) => format!("({} & {})", a.pretty(), b.pretty()),
      Type::Top => "<top>".to_string(),
      Type::Variable(variable) => variable.clone(),
      Type::Recursive(name, typ) => format!("(μ {} . {})", name, typ.pretty()),
      Type::RecursiveVariable(variable) => variable.clone(),
      Type::Object(map) => render_object(":", map.iter()),
    }
  }
}

impl Type {
  fn visit<F>(&self, f: &mut F)
  where
    F: FnMut(Polarity, &Type),
  {
    self.visit_helper(Polarity::Positive, f);
  }

  fn visit_helper<F>(&self, polarity: Polarity, f: &mut F)
  where
    F: FnMut(Polarity, &Type),
  {
    match self {
      Type::String => {}
      Type::Function(a, b) => {
        a.visit_helper(polarity.swap(), f);
        b.visit_helper(polarity, f);
      }
      Type::Object(map) => {
        for field in map.values() {
          field.visit_helper(polarity, f);
        }
      }
      Type::Union(a, b) => {
        a.visit_helper(polarity, f);
        b.visit_helper(polarity, f);
      }
      Type::Bottom => {}
      Type::Intersection(a, b) => {
        a.visit_helper(polarity, f);
        b.visit_helper(polarity, f);
      }
      Type::Top => {}
      Type::Variable(_) => {}
      Type::Recursive(_, inner) => {
        inner.visit_helper(polarity, f);
      }
      Type::RecursiveVariable(_) => {}
    }
    f(polarity, self);
  }

  fn visit_mut<F>(&mut self, f: &mut F)
  where
    F: FnMut(&mut Type),
  {
    match self {
      Type::String => {}
      Type::Function(a, b) => {
        a.visit_mut(f);
        b.visit_mut(f);
      }
      Type::Object(map) => {
        for field in map.values_mut() {
          field.visit_mut(f);
        }
      }
      Type::Union(a, b) => {
        a.visit_mut(f);
        b.visit_mut(f);
      }
      Type::Bottom => {}
      Type::Intersection(a, b) => {
        a.visit_mut(f);
        b.visit_mut(f);
      }
      Type::Top => {}
      Type::Variable(_) => {}
      Type::Recursive(_, inner) => {
        inner.visit_mut(f);
      }
      Type::RecursiveVariable(_) => {}
    }
    f(self);
  }

  fn extract_variables(&mut self) -> (HashSet<String>, HashSet<String>) {
    let mut positives = HashSet::new();
    let mut negatives = HashSet::new();
    self.visit(&mut |p, typ| {
      if let Type::Variable(variable) = typ {
        match p {
          Polarity::Positive => {
            positives.insert(variable.to_string());
          }
          Polarity::Negative => {
            negatives.insert(variable.to_string());
          }
        }
      }
    });
    (positives, negatives)
  }

  fn replace_variable(&mut self, needle: &str, replacement: Type) {
    self.visit_mut(&mut |typ| match typ {
      Type::Variable(variable) if variable == needle => {
        *typ = replacement.clone();
      }
      _ => {}
    });
  }

  fn simplify_intersections_and_unions(&mut self) {
    self.visit_mut(&mut |typ| match typ {
      Type::Union(a, b) if **a == Type::Bottom => {
        *typ = *b.clone();
      }
      Type::Union(a, b) if **b == Type::Bottom => {
        *typ = *a.clone();
      }
      Type::Union(a, b) if a == b => {
        *typ = *a.clone();
      }
      Type::Intersection(a, b) if **a == Type::Top => {
        *typ = *b.clone();
      }
      Type::Intersection(a, b) if **b == Type::Top => {
        *typ = *a.clone();
      }
      Type::Intersection(a, b) if a == b => {
        *typ = *a.clone();
      }
      _ => {}
    });
  }

  fn simplify_polars(&mut self) {
    let (positives, negatives) = self.extract_variables();
    for polar_negative in negatives.difference(&positives) {
      self.replace_variable(polar_negative, Type::Top);
    }
    for polar_positive in positives.difference(&negatives) {
      self.replace_variable(polar_positive, Type::Bottom);
    }
  }

  pub fn simplify(mut self) -> Type {
    self.simplify_polars();
    self.simplify_intersections_and_unions();
    self
  }

  fn assert_dispatchable_intersection_function(&self) -> R<()> {
    match self {
      Type::Function(a, _) => a.assert_dispatchable_input_type(),
      Type::Bottom => Err(
        format!(
          "cannot use value of type {} in intersection",
          Type::Bottom.pretty()
        )
        .into(),
      ),
      _ => panic!("type checker bug"),
    }
  }

  fn assert_dispatchable_input_type(&self) -> R<()> {
    use Type::*;
    match self {
      Function(_, _) => Err(
        format!(
          "cannot use {} as parameter type to function intersection",
          self.pretty()
        )
        .into(),
      ),
      Variable(_) => {
        Err("cannot use type variable as parameter type to function intersection".into())
      }
      RecursiveVariable(_) => Ok(()),
      Union(a, b) | Intersection(a, b) => {
        a.assert_dispatchable_input_type()?;
        b.assert_dispatchable_input_type()?;
        Ok(())
      }
      Object(map) => {
        for field in map.values() {
          field.assert_dispatchable_input_type()?;
        }
        Ok(())
      }
      Recursive(_, inner) => inner.assert_dispatchable_input_type(),
      String | Top | Bottom => Ok(()),
    }
  }
}

impl Ast<Type> {
  fn assert_dispatchability(&self) -> R<()> {
    self.visit(&mut |ast| match &ast.ast {
      AstNode::Intersection(a, b) => {
        a.typ.assert_dispatchable_intersection_function()?;
        b.typ.assert_dispatchable_intersection_function()?;
        Ok(())
      }
      _ => Ok(()),
    })
  }
}
