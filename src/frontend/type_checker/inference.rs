#[cfg(test)]
mod test;

use super::dependency_analysis::{group, sort_same_as};
use super::internal_type::Polarity;
use super::{
  internal_type::{InternalType, VariableState},
  name_resolution,
  name_resolution::AstNode,
};
use crate::builtins::builtin_type;
use crate::utils::map::Map;
use crate::utils::pretty::Pretty;
use crate::{frontend::name_resolution::Id, utils::errors::R};
use indexmap::IndexMap;
use std::fmt::Debug;
use std::{
  collections::{HashMap, HashSet},
  iter::once,
};

#[derive(Debug, PartialEq)]
pub struct Ast<T> {
  pub ast: AstNode<Ast<T>>,
  pub typ: T,
}

impl<T> Pretty for Ast<T> {
  fn pretty(&self) -> String {
    self.ast.pretty()
  }
}

impl<T> Ast<T> {
  pub fn map<F, U>(self, f: &mut F) -> Ast<U>
  where
    F: FnMut(T) -> U,
  {
    Ast {
      ast: self.ast.map(|ast: Ast<T>| ast.map(f)),
      typ: f(self.typ),
    }
  }

  pub fn visit<F>(&self, f: &mut F) -> R<()>
  where
    F: FnMut(&Ast<T>) -> R<()>,
    T: Debug,
  {
    use AstNode::*;
    match &self.ast {
      Accessor(expression, _) => expression.visit(f)?,
      Object(map) => {
        for field in map.values() {
          field.visit(f)?;
        }
      }
      Intersection(a, b) => {
        a.visit(f)?;
        b.visit(f)?;
      }
      Lambda {
        parameter: _,
        parameter_type: _,
        body,
      } => body.visit(f)?,
      StringLiteral(_) => {}
      Identifier(_) => {}
      Application(a, b) => {
        a.visit(f)?;
        b.visit(f)?;
      }
      Typed(expression, _) => {
        expression.visit(f)?;
      }
      Builtin(_) => {}
    }
    f(self)?;
    Ok(())
  }
}

#[derive(Debug, Clone)]
pub enum TypeScheme {
  MonomorphicType(InternalType),
  PolymorphicType {
    level: i64,
    internal_type: InternalType,
  },
}

impl TypeScheme {
  fn instantiate(&self, new_level: i64) -> InternalType {
    match self {
      TypeScheme::MonomorphicType(internal_type) => internal_type.clone(),
      TypeScheme::PolymorphicType {
        level,
        internal_type,
      } => internal_type.freshen_above(*level, new_level),
    }
  }
}

pub fn type_term(ast: name_resolution::Ast) -> R<Ast<InternalType>> {
  type_term_helper(ast, &HashMap::new(), 0, &mut HashSet::new())
}

fn type_term_helper(
  ast: name_resolution::Ast,
  context: &HashMap<Id, TypeScheme>,
  current_level: i64,
  cache: &mut HashSet<(InternalType, InternalType)>,
) -> R<Ast<InternalType>> {
  Ok(match ast.0 {
    AstNode::StringLiteral(string) => Ast {
      ast: AstNode::StringLiteral(string),
      typ: InternalType::String,
    },
    AstNode::Application(f, x) => {
      let result = InternalType::Variable(VariableState::fresh_var(current_level));
      let f = type_term_helper(*f, context, current_level, cache)?;
      let x = type_term_helper(*x, context, current_level, cache)?;
      constrain(
        &f.typ,
        &InternalType::Function(Box::new(x.typ.clone()), Box::new(result.clone())),
        cache,
      )?;
      Ast {
        ast: AstNode::Application(Box::new(f), Box::new(x)),
        typ: result,
      }
    }
    AstNode::Lambda {
      parameter,
      parameter_type,
      body,
    } => {
      let param = match &parameter_type {
        None => InternalType::Variable(VariableState::fresh_var(current_level)),
        Some(typ) => InternalType::new(typ),
      };
      let mut new_context = context.clone();
      new_context.insert(
        parameter.clone(),
        TypeScheme::MonomorphicType(param.clone()),
      );
      let body = type_term_helper(*body, &new_context, current_level, cache)?;
      let typ = InternalType::Function(Box::new(param), Box::new(body.typ.clone()));
      Ast {
        ast: AstNode::Lambda {
          parameter,
          parameter_type,
          body: Box::new(body),
        },
        typ,
      }
    }
    AstNode::Identifier(id) => match context.get(&id) {
      Some(type_scheme) => Ast {
        ast: AstNode::Identifier(id),
        typ: type_scheme.instantiate(current_level),
      },
      None => return Err(format!("not found: {}", id.name).into()),
    },
    AstNode::Object(mut map) => {
      let keys: Vec<Id> = map.keys().cloned().collect();
      let mut new_context = context.clone();
      let mut fresh_field_variables = HashMap::new();
      let mut object_type = Map::new();
      let groups = group(&map);
      let mut level_increment = groups.len() as i64;
      let mut object = IndexMap::new();
      for group in groups {
        for id in group.iter() {
          let new_variable_state = VariableState::fresh_var(current_level + level_increment);
          new_context.insert(
            id.clone(),
            TypeScheme::MonomorphicType(InternalType::Variable(new_variable_state.clone())),
          );
          fresh_field_variables.insert(id.clone(), new_variable_state);
        }
        for id in group.iter() {
          let field_ast = map.remove(id).unwrap();
          let field = type_term_helper(
            field_ast,
            &new_context,
            current_level + level_increment,
            cache,
          )?;
          constrain(
            &field.typ,
            &InternalType::Variable(fresh_field_variables.get(id).unwrap().clone()),
            cache,
          )?;
          object_type.insert(id.name.to_string(), field.typ.clone());
          object.insert(id.clone(), field);
        }
        for id in group.iter() {
          new_context.insert(
            id.clone(),
            TypeScheme::PolymorphicType {
              level: current_level,
              internal_type: object_type.get(&id.name).unwrap().clone(),
            },
          );
        }
        level_increment -= 1;
      }
      let object_type = sort_same_as(object_type, &mut keys.iter().map(|id| &id.name));
      Ast {
        ast: AstNode::Object(object),
        typ: InternalType::Object(object_type),
      }
    }
    AstNode::Builtin(builtin_name) => Ast {
      ast: AstNode::Builtin(builtin_name),
      typ: builtin_type(&builtin_name),
    },
    AstNode::Typed(expression, typ) => {
      let expression = type_term_helper(*expression, context, current_level, cache)?;
      let ascription_type = InternalType::new(&typ);
      constrain(&expression.typ, &ascription_type, cache)?;
      Ast {
        ast: AstNode::Typed(
          Box::new(Ast {
            ast: expression.ast,
            typ: expression.typ,
          }),
          typ,
        ),
        typ: ascription_type,
      }
    }
    AstNode::Accessor(expression, accessor) => {
      let field_type = InternalType::Variable(VariableState::fresh_var(current_level));
      let expression = type_term_helper(*expression, context, current_level, cache)?;
      constrain(
        &expression.typ,
        &InternalType::Object(once((accessor.clone(), field_type.clone())).collect()),
        cache,
      )?;
      Ast {
        ast: AstNode::Accessor(Box::new(expression), accessor),
        typ: field_type,
      }
    }
    AstNode::Intersection(left, right) => {
      let left = type_term_helper(*left, context, current_level, cache)?;
      let right = type_term_helper(*right, context, current_level, cache)?;
      let left_input = InternalType::Variable(VariableState::fresh_var(current_level));
      let right_input = InternalType::Variable(VariableState::fresh_var(current_level));
      let return_type = InternalType::Variable(VariableState::fresh_var(current_level));
      constrain(
        &left.typ,
        &InternalType::Function(Box::new(left_input.clone()), Box::new(return_type.clone())),
        cache,
      )
      .map_err(|_: CannotConstrain| "intersections are only supported for functions")?;
      constrain(
        &right.typ,
        &InternalType::Function(Box::new(right_input.clone()), Box::new(return_type.clone())),
        cache,
      )
      .map_err(|_: CannotConstrain| "intersections are only supported for functions")?;
      Ast {
        ast: AstNode::Intersection(Box::new(left), Box::new(right)),
        typ: InternalType::Function(
          Box::new(InternalType::Union(
            Box::new(left_input),
            Box::new(right_input),
          )),
          Box::new(return_type),
        ),
      }
    }
  })
}

#[derive(Debug)]
struct CannotConstrain {
  lhs: InternalType,
  rhs: InternalType,
  missing_fields: HashSet<String>,
}

impl std::fmt::Display for CannotConstrain {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(
      f,
      "cannot constrain {} <: {}",
      self.lhs.coalesce_type().simplify().pretty(),
      self.rhs.coalesce_type().simplify().pretty()
    )?;
    for missing_field in self.missing_fields.iter() {
      write!(f, "\n(missing field: {})", missing_field,)?;
    }
    Ok(())
  }
}

impl std::error::Error for CannotConstrain {}

fn constrain(
  lhs: &InternalType,
  rhs: &InternalType,
  cache: &mut HashSet<(InternalType, InternalType)>,
) -> Result<(), CannotConstrain> {
  if cache.contains(&(lhs.clone(), rhs.clone())) {
    return Ok(());
  } else {
    cache.insert((lhs.clone(), rhs.clone()));
  }
  match (lhs, rhs) {
    (InternalType::String, InternalType::String) => {}
    (InternalType::Function(a_input, a_output), InternalType::Function(b_input, b_output)) => {
      constrain(b_input, a_input, cache)?;
      constrain(a_output, b_output, cache)?;
    }
    (InternalType::Object(a), InternalType::Object(b)) => {
      for (key, b_field) in b.iter() {
        match a.get(key) {
          Some(a_field) => constrain(a_field, b_field, cache)?,
          None => {
            return Err(CannotConstrain {
              lhs: lhs.clone(),
              rhs: rhs.clone(),
              missing_fields: once(key.clone()).collect(),
            })
          }
        }
      }
    }
    (InternalType::Variable(_), rhs) if rhs.level() > lhs.level() => {
      let rhs = rhs.extrude(Polarity::Negative, lhs.level());
      constrain(lhs, &rhs, cache)?;
    }
    (InternalType::Variable(lhs), rhs) => {
      lhs
        .upper_bounds
        .access(|upper_bounds| upper_bounds.push(rhs.clone()));
      for lhs_lower_bound in lhs.lower_bounds.iter_cloned() {
        constrain(&lhs_lower_bound, rhs, cache)?;
      }
    }
    (lhs, InternalType::Variable(_)) if lhs.level() > rhs.level() => {
      let lhs = lhs.extrude(Polarity::Positive, rhs.level());
      constrain(&lhs, rhs, cache)?;
    }
    (lhs, InternalType::Variable(rhs)) => {
      rhs
        .lower_bounds
        .access(|lower_bounds| lower_bounds.push(lhs.clone()));
      for rhs_upper_bound in rhs.upper_bounds.iter_cloned() {
        constrain(lhs, &rhs_upper_bound, cache)?;
      }
    }
    (InternalType::Union(a, b), rhs) => {
      constrain(a, rhs, cache)?;
      constrain(b, rhs, cache)?;
    }
    (lhs, InternalType::Union(a, b)) => match try_constrain(lhs, a, cache) {
      Ok(()) => {
        // this is not tested
        constrain(lhs, a, cache)?;
      }
      Err(a_error) => {
        constrain(lhs, b, cache).map_err(|b_error| CannotConstrain {
          lhs: lhs.clone(),
          rhs: InternalType::Union(Box::new(a_error.rhs), Box::new(b_error.rhs)),
          missing_fields: a_error
            .missing_fields
            .union(&b_error.missing_fields)
            .cloned()
            .collect(),
        })?;
      }
    },
    (lhs, rhs) => {
      return Err(CannotConstrain {
        lhs: lhs.clone(),
        rhs: rhs.clone(),
        missing_fields: HashSet::new(),
      });
    }
  }
  Ok(())
}

fn try_constrain(
  lhs: &InternalType,
  rhs: &InternalType,
  cache: &mut HashSet<(InternalType, InternalType)>,
) -> Result<(), CannotConstrain> {
  let clone_map = &mut HashMap::new();
  let mut cache_clone = cache
    .iter()
    .map(|(a, b)| (a.deep_clone(clone_map), b.deep_clone(clone_map)))
    .collect();
  constrain(
    &lhs.deep_clone(clone_map),
    &rhs.deep_clone(clone_map),
    &mut cache_clone,
  )
}
