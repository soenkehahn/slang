use std::str::Chars;

pub fn strip_comments(code: &str) -> StripComments {
  StripComments::new(code)
}

pub struct StripComments<'a> {
  chars: Chars<'a>,
}

impl<'a> StripComments<'a> {
  fn new(code: &str) -> StripComments {
    StripComments {
      chars: code.chars(),
    }
  }
}

impl<'a> Iterator for StripComments<'a> {
  type Item = char;

  fn next(&mut self) -> Option<char> {
    match self.chars.next() {
      Some('#') => {
        for char in self.chars.by_ref() {
          if char == '\n' {
            return Some('\n');
          }
        }
        None
      }
      next => next,
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;

  fn test(code: &str, expected: &str) {
    assert_eq!(strip_comments(code).collect::<String>(), expected);
  }

  #[test]
  fn removes_single_line_comments() {
    test("foo\n# bar\nbaz\n", "foo\n\nbaz\n");
  }

  #[test]
  fn removes_single_line_comments_after_spaces() {
    test("foo\n   # bar\nbaz\n", "foo\n   \nbaz\n");
  }

  #[test]
  fn removes_trailing_comments() {
    test("foo # bar\n", "foo \n");
  }
}
