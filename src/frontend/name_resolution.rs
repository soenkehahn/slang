use crate::builtins::resolve_builtin;
use crate::frontend::parser;
use crate::utils::pretty::render_object;
use crate::utils::pretty::Pretty;
use crate::R;
use crate::{builtins::BuiltinName, frontend::parser::types::Type as ParsedType};
use indexmap::IndexMap;
use std::fmt::Debug;
use std::iter::once;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct Id {
  pub name: String,
  pub id: i64,
}

#[derive(Debug, PartialEq, Clone)]
pub enum Type {
  String,
  Function(Box<Type>, Box<Type>),
  Object(IndexMap<String, Type>),
  Union(Box<Type>, Box<Type>),
}

impl Pretty for Type {
  fn pretty(&self) -> String {
    use Type::*;
    match self {
      String => "string".to_string(),
      Object(map) => render_object(":", map.iter()),
      Function(a, b) => format!("{} -> {}", a.pretty(), b.pretty()),
      Union(a, b) => format!("{} | {}", a.pretty(), b.pretty()),
    }
  }
}

#[derive(Debug, PartialEq)]
pub enum AstNode<Child> {
  Object(IndexMap<Id, Child>),
  Identifier(Id),
  StringLiteral(String),
  Application(Box<Child>, Box<Child>),
  Accessor(Box<Child>, String),
  Lambda {
    parameter: Id,
    parameter_type: Option<Type>,
    body: Box<Child>,
  },
  Builtin(BuiltinName),
  Typed(Box<Child>, Type),
  Intersection(Box<Child>, Box<Child>),
}

impl<Child> AstNode<Child> {
  pub fn map<F, NewChild>(self, mut f: F) -> AstNode<NewChild>
  where
    F: FnMut(Child) -> NewChild,
  {
    use AstNode::*;

    match self {
      AstNode::Object(map) => Object(
        map
          .into_iter()
          .map(|(key, value)| (key, f(value)))
          .collect(),
      ),
      Identifier(id) => Identifier(id),
      StringLiteral(string) => StringLiteral(string),
      Application(a, b) => Application(Box::new(f(*a)), Box::new(f(*b))),
      Accessor(expression, accessor) => Accessor(Box::new(f(*expression)), accessor),
      Lambda {
        parameter,
        parameter_type,
        body,
      } => Lambda {
        parameter,
        parameter_type,
        body: Box::new(f(*body)),
      },
      Builtin(name) => Builtin(name),
      Typed(expression, typ) => Typed(Box::new(f(*expression)), typ),
      Intersection(a, b) => Intersection(Box::new(f(*a)), Box::new(f(*b))),
    }
  }
}

impl<Child: Pretty> Pretty for AstNode<Child> {
  fn pretty(&self) -> String {
    match self {
      AstNode::Identifier(id) => id.name.clone(),
      AstNode::Object(map) => render_object(" =", map.iter().map(|(id, field)| (&id.name, field))),
      AstNode::Application(a, b) => format!("({} {})", a.pretty(), b.pretty()),
      AstNode::Lambda {
        parameter,
        parameter_type: None,
        body,
      } => format!("\\ {} . {}", parameter.name, body.pretty()),
      AstNode::StringLiteral(string) => format!("\"{}\"", string),
      AstNode::Typed(expression, typ) => format!("{} : {}", expression.pretty(), typ.pretty()),
      AstNode::Accessor(expression, accessor) => format!("{}.{}", expression.pretty(), accessor),
      AstNode::Builtin(name) => name.pretty(),
      AstNode::Lambda {
        parameter,
        parameter_type,
        body,
      } => {
        let parameter_type = match parameter_type {
          Some(typ) => format!(": {}", typ.pretty()),
          None => "".to_string(),
        };
        format!(
          "\\ {}{} . {}",
          parameter.name,
          parameter_type,
          body.pretty()
        )
      }
      AstNode::Intersection(a, b) => format!("({} & {})", a.pretty(), b.pretty()),
    }
  }
}

#[derive(Debug, PartialEq)]
pub struct Ast(pub AstNode<Ast>);

impl Ast {
  pub fn collect_identifiers(&self) -> Vec<Id> {
    use AstNode::*;
    let mut result = vec![];
    let mut queue = vec![&self.0];
    while let Some(ast) = queue.pop() {
      match ast {
        Identifier(id) => result.push(id.clone()),
        Builtin(_) => {}
        Lambda {
          parameter,
          parameter_type: _,
          body,
        } => {
          result.push(parameter.clone());
          queue.push(&body.0);
        }
        Object(map) => {
          for (id, field) in map.iter() {
            result.push(id.clone());
            queue.push(&field.0);
          }
        }
        Application(a, b) => {
          queue.push(&a.0);
          queue.push(&b.0);
        }
        StringLiteral(_) => {}
        Accessor(object, _) => queue.push(&object.0),
        Typed(expression, _) => queue.push(&expression.0),
        Intersection(a, b) => {
          queue.push(&a.0);
          queue.push(&b.0);
        }
      }
    }
    result
  }
}

pub fn run(ast: parser::Ast) -> R<Ast> {
  Resolver::new().resolve(ast)
}

struct Resolver {
  id_counter: i64,
  scopes: Vec<IndexMap<String, i64>>,
}

impl Resolver {
  fn new() -> Resolver {
    Resolver {
      id_counter: 0,
      scopes: vec![],
    }
  }

  fn new_id(&mut self) -> i64 {
    let result = self.id_counter;
    self.id_counter += 1;
    result
  }

  fn find_in_scopes(&self, identifier: &str) -> R<Ast> {
    for scope in self.scopes.iter().rev() {
      if let Some(found) = scope.get(identifier) {
        return Ok(Ast(AstNode::Identifier(Id {
          name: identifier.to_string(),
          id: *found,
        })));
      }
    }
    Err(format!("'{}' not found", identifier).into())
  }

  fn resolve(&mut self, parsed: parser::Ast) -> R<Ast> {
    Ok(Ast(match parsed {
      parser::Ast::Identifier(identifier) => match resolve_builtin(&identifier) {
        Some(builtin) => AstNode::Builtin(builtin),
        None => self.find_in_scopes(&identifier)?.0,
      },
      parser::Ast::Object(map) => {
        let object_scope: IndexMap<String, i64> =
          map.keys().map(|key| (key.clone(), self.new_id())).collect();
        let mut result: IndexMap<Id, Ast> = IndexMap::new();
        self.scopes.push(object_scope.clone());
        for (key, parsed) in map.into_iter() {
          let id = object_scope
            .get(&key)
            .expect("Resolver::resolve: key mismatch");
          let resolved_field = self.resolve(parsed)?;
          let id = Id { id: *id, name: key };
          result.insert(id, resolved_field);
        }
        self.scopes.pop();
        AstNode::Object(result)
      }
      parser::Ast::Lambda(parameter_name, typ, body) => {
        let parameter_id = self.new_id();
        let parameter_scope = once((parameter_name.clone(), parameter_id)).collect();
        self.scopes.push(parameter_scope);
        let resolved_body = Box::new(self.resolve(*body)?);
        self.scopes.pop();
        AstNode::Lambda {
          parameter: Id {
            name: parameter_name,
            id: parameter_id,
          },
          parameter_type: typ.map(Resolver::resolve_type).transpose()?,
          body: resolved_body,
        }
      }
      parser::Ast::Application(a, b) => {
        AstNode::Application(Box::new(self.resolve(*a)?), Box::new(self.resolve(*b)?))
      }
      parser::Ast::Accessor(object, accessor) => {
        AstNode::Accessor(Box::new(self.resolve(*object)?), accessor)
      }
      parser::Ast::StringLiteral(string) => AstNode::StringLiteral(string),
      parser::Ast::Typed(expression, typ) => AstNode::Typed(
        Box::new(self.resolve(*expression)?),
        Resolver::resolve_type(typ)?,
      ),
      parser::Ast::Intersection(a, b) => {
        AstNode::Intersection(Box::new(self.resolve(*a)?), Box::new(self.resolve(*b)?))
      }
    }))
  }

  fn resolve_type(typ: parser::types::Type) -> R<Type> {
    Ok(match typ {
      ParsedType::Identifier(identifier) => match identifier.as_str() {
        "string" => Type::String,
        _ => return Err(format!("unknown type: {}", identifier).into()),
      },
      ParsedType::Function(a, b) => Type::Function(
        Box::new(Resolver::resolve_type(*a)?),
        Box::new(Resolver::resolve_type(*b)?),
      ),
      ParsedType::Object(map) => {
        let mut result = IndexMap::new();
        for (key, value) in map.into_iter() {
          result.insert(key, Resolver::resolve_type(value)?);
        }
        Type::Object(result)
      }
      ParsedType::Union(a, b) => Type::Union(
        Box::new(Resolver::resolve_type(*a)?),
        Box::new(Resolver::resolve_type(*b)?),
      ),
    })
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::frontend::tokenizer;

  mod type_pretty {
    use super::*;
    use Type::*;

    #[test]
    fn renders_types() {
      assert_eq!(
        Object(
          vec![
            ("a".to_string(), String),
            (
              "b".to_string(),
              Function(Box::new(String), Box::new(String))
            )
          ]
          .into_iter()
          .collect()
        )
        .pretty(),
        "{ a: string; b: string -> string; }"
      );
    }
  }

  fn test(code: &str) -> R<Ast> {
    run(parser::run(tokenizer::run(code))?)
  }

  impl Ast {
    fn panic<T>(&self, expected: &str) -> T {
      panic!("expect_object: {}, got {:?}", expected, self)
    }

    fn expect_object(&self, key: &str) -> (i64, &Ast) {
      match &self.0 {
        AstNode::Object(map) => {
          let matching: Vec<(&Id, &Ast)> = map.iter().filter(|(id, _)| id.name == key).collect();
          match matching[..] {
            [(id, ast)] => (id.id, ast),
            [] => self.panic(&format!("object with field '{}'", key)),
            [_, _, ..] => self.panic("object with unique fields"),
          }
        }
        _ => self.panic("object"),
      }
    }

    fn expect_object_id(&self, key: &str) -> i64 {
      self.expect_object(key).0
    }

    fn expect_object_ast(&self, key: &str) -> &Ast {
      self.expect_object(key).1
    }

    fn expect_identifier(&self) -> i64 {
      match &self.0 {
        AstNode::Identifier(id) => id.id,
        _ => self.panic("identifier"),
      }
    }

    fn expect_lambda(&self) -> (i64, &Ast) {
      match &self.0 {
        AstNode::Lambda {
          parameter,
          parameter_type: _,
          body,
        } => (parameter.id, body),
        _ => self.panic("lambda"),
      }
    }
  }

  mod object {
    use super::*;

    #[test]
    fn resolves_sibling_fields() -> R<()> {
      let ast = test("{bar = \"\"; foo = bar;}")?;
      assert_eq!(
        ast.expect_object_id("bar"),
        ast.expect_object_ast("foo").expect_identifier()
      );
      Ok(())
    }

    #[test]
    fn uses_unique_ids() -> R<()> {
      let ast = test("{foo = \"\"; bar = \"\";}")?;
      assert_ne!(ast.expect_object_id("foo"), ast.expect_object_id("bar"));
      Ok(())
    }

    #[test]
    fn resolves_sibling_fields_in_different_order() -> R<()> {
      let ast = test("{foo = bar; bar = \"\";}")?;
      assert_eq!(
        ast.expect_object_id("bar"),
        ast.expect_object_ast("foo").expect_identifier()
      );
      Ok(())
    }

    #[test]
    fn pops_scopes_off_the_stack_correctly() {
      let ast = test(
        "{
          a = {
            foo = \"foo\";
          };
          b = {
            bar = foo;
          };
        }",
      );
      assert_eq!(
        ast.expect_err("no name resolution error").to_string(),
        "'foo' not found"
      );
    }
  }

  mod lambdas {
    use super::*;

    #[test]
    fn resolves_lambda_parameters() -> R<()> {
      let ast = test("\\ x . x")?;
      let (parameter, body) = ast.expect_lambda();
      assert_eq!(parameter, body.expect_identifier());
      Ok(())
    }

    #[test]
    fn resolves_nested_lambda_parameters() -> R<()> {
      let ast = test("\\ x . {foo = x;}")?;
      let (parameter, body) = ast.expect_lambda();
      assert_eq!(parameter, body.expect_object_ast("foo").expect_identifier());
      Ok(())
    }

    #[test]
    fn resolves_outer_siblings_in_lambdas() -> R<()> {
      let ast = test("{ foo = \"foo\"; f = \\ x . foo; }")?;
      let foo_reference = ast.expect_object_id("foo");
      let f = &ast.expect_object_ast("f");
      let (_parameter, body) = f.expect_lambda();
      assert_eq!(foo_reference, body.expect_identifier());
      Ok(())
    }

    #[test]
    fn resolves_inner_siblings_in_lambdas() -> R<()> {
      let ast = test("\\ x . { foo = \"foo\"; bar = foo; }")?;
      let (_parameter, body) = ast.expect_lambda();
      let foo_reference = body.expect_object_id("foo");
      assert_eq!(
        foo_reference,
        body.expect_object_ast("bar").expect_identifier()
      );
      Ok(())
    }

    #[test]
    fn shadows_outer_siblings_by_parameters() -> R<()> {
      let ast = test("{ x = \"x\"; f = \\ x . x; }")?;
      let f = &ast.expect_object_ast("f");
      let (parameter, body) = f.expect_lambda();
      assert_eq!(body.expect_identifier(), parameter);
      Ok(())
    }

    #[test]
    fn shadows_parameters_by_inner_siblings() -> R<()> {
      let ast = test("\\ x . { x = \"x\"; foo = x; }")?;
      let (_parameter, body) = ast.expect_lambda();
      let x_reference = body.expect_object_id("x");
      assert_eq!(
        x_reference,
        body.expect_object_ast("foo").expect_identifier()
      );
      Ok(())
    }

    #[test]
    fn pops_the_parameter_scope_after_exiting_lambda_body() {
      let ast = test(
        r#"{
          a = \ x . x;
          b = x;
        }"#,
      );
      assert_eq!(
        ast.expect_err("no name resolution error").to_string(),
        "'x' not found"
      );
    }
  }
}
