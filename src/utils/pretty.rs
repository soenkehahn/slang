use std::sync::Arc;

pub trait Pretty {
  fn pretty(&self) -> String;
}

impl<T: Pretty> Pretty for &T {
  fn pretty(&self) -> String {
    T::pretty(self)
  }
}

impl<T: Pretty> Pretty for Arc<T> {
  fn pretty(&self) -> String {
    T::pretty(self)
  }
}

impl<T: Pretty> Pretty for Vec<T> {
  fn pretty(&self) -> String {
    format!(
      "[{}]",
      self
        .iter()
        .map(|t| t.pretty())
        .collect::<Vec<_>>()
        .join(", ")
    )
  }
}

pub fn render_object<'a, Element, Iter>(equal: &str, map: Iter) -> String
where
  Element: Pretty,
  Iter: Iterator<Item = (&'a String, Element)>,
{
  let mut lines = vec!["{".to_string()];
  for (key, value) in map {
    lines.push(format!("  {}{} {};", key, equal, indent(&value.pretty())));
  }
  if lines.len() == 1 {
    return "{}".to_string();
  }
  lines.push("}".to_string());
  let len = lines.iter().fold(0, |acc, next| acc + next.len());
  if len < 40 {
    lines
      .into_iter()
      .map(|line| line.trim().to_string())
      .collect::<Vec<String>>()
      .join(" ")
  } else {
    lines.join("\n")
  }
}

fn indent(input: &str) -> String {
  let mut result = String::new();
  for char in input.chars() {
    result.push(char);
    if char == '\n' {
      result.push_str("  ");
    }
  }
  result
}
