pub type R<T> = Result<T, Box<dyn std::error::Error>>;

pub type RS<T> = Result<T, String>;

#[cfg(test)]
pub trait ResultExt {
  type T;

  fn into_rs(self) -> RS<Self::T>;
}

#[cfg(test)]
impl<T> ResultExt for R<T> {
  type T = T;

  fn into_rs(self) -> RS<Self::T> {
    self.map_err(|err| err.to_string())
  }
}

pub trait OptionExt {
  type T;
  type E;
  fn transpose_ref(&self) -> Result<Option<&Self::T>, Self::E>;
}

impl<T, E: Clone> OptionExt for Option<&Result<T, E>> {
  type T = T;
  type E = E;

  fn transpose_ref(&self) -> Result<Option<&Self::T>, Self::E> {
    match self {
      None => Ok(None),
      Some(Ok(t)) => Ok(Some(t)),
      Some(Err(err)) => Err(err.clone()),
    }
  }
}
