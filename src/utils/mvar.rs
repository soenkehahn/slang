use super::pretty::Pretty;
use std::sync::{Arc, Mutex};
use std::{fmt::Debug, sync::TryLockError};
use std::{
  hash::{Hash, Hasher},
  ops::DerefMut,
};

#[derive(Clone)]
pub struct MVar<T>(Arc<Mutex<T>>);

impl<T: Debug> Debug for MVar<T> {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match self.0.try_lock() {
      Ok(t) => {
        let t: &T = &*t;
        f.debug_struct("MVar")
          .field("ptr", &Arc::as_ptr(&self.0))
          .field("value", t)
          .finish()
      }
      Err(TryLockError::WouldBlock) => write!(f, "<locked({:?})>", Arc::as_ptr(&self.0)),
      Err(TryLockError::Poisoned(err)) => panic!("{}", err.to_string()),
    }
  }
}

impl<T> PartialEq for MVar<T> {
  fn eq(&self, other: &Self) -> bool {
    Arc::as_ptr(&self.0) == Arc::as_ptr(&other.0)
  }
}

impl<T> Eq for MVar<T> {}

impl<T> Hash for MVar<T> {
  fn hash<H: Hasher>(&self, state: &mut H) {
    Arc::as_ptr(&self.0).hash(state);
  }
}

impl<T: Pretty> Pretty for MVar<T> {
  fn pretty(&self) -> String {
    self.try_access(|| "<locked>".to_string(), |t| t.pretty())
  }
}

impl<T> MVar<T> {
  pub fn new(t: T) -> MVar<T> {
    MVar(Arc::new(Mutex::new(t)))
  }

  pub fn try_access<Default, F, U>(&self, default: Default, f: F) -> U
  where
    F: FnOnce(&mut T) -> U,
    Default: FnOnce() -> U,
  {
    match self.0.try_lock() {
      Ok(mut lock) => f(lock.deref_mut()),
      Err(TryLockError::WouldBlock) => default(),
      Err(err) => Err(err).unwrap(),
    }
  }

  pub fn access<F, U>(&self, f: F) -> U
  where
    F: FnOnce(&mut T) -> U,
  {
    self.try_access(|| panic!("mvar is blocked"), f)
  }
}

pub struct IterCloned<T>(usize, MVar<Vec<T>>);

impl<T: Clone> Iterator for IterCloned<T> {
  type Item = T;

  fn next(&mut self) -> Option<T> {
    match self.1.access(|vec| vec.get(self.0).cloned()) {
      Some(element) => {
        self.0 += 1;
        Some(element)
      }
      None => None,
    }
  }
}

impl<T: Clone> MVar<Vec<T>> {
  pub fn iter_cloned(&self) -> IterCloned<T> {
    IterCloned(0, self.clone())
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use std::collections::hash_map::DefaultHasher;

  #[test]
  #[allow(clippy::eq_op)]
  fn variables_are_equal_by_pointer_equality() {
    assert_ne!(MVar::new(()), MVar::new(()));
    let variable_state = MVar::new(());
    assert_eq!(variable_state, variable_state);
  }

  fn hash<T: Hash>(x: &T) -> u64 {
    let mut hasher = DefaultHasher::new();
    x.hash(&mut hasher);
    hasher.finish()
  }

  #[test]
  fn same_variable_states_have_different_identities_by_hash() {
    assert_ne!(hash(&MVar::new(())), hash(&MVar::new(())))
  }

  #[test]
  fn identical_mvars_have_the_same_hash() {
    let mvar = MVar::new(());
    assert_eq!(hash(&mvar), hash(&mvar))
  }

  mod access {
    use super::*;

    #[test]
    fn allows_to_modify_the_contained_value() {
      let mvar = MVar::new(0);
      mvar.access(|x| *x += 1);
      assert_eq!(*mvar.0.try_lock().unwrap(), 1);
    }

    #[test]
    fn allows_to_retrieve_the_value_by_cloning() {
      #[derive(Clone, Debug, PartialEq)]
      struct Wrapper(i32);
      let mvar = MVar::new(Wrapper(42));
      let value = mvar.access(|x| x.clone());
      assert_eq!(value, Wrapper(42));
    }
  }

  mod try_access {
    use super::*;

    #[test]
    fn allows_to_access() {
      let mvar = MVar::new(0);
      mvar.try_access(|| panic!("not used"), |x| *x += 1);
      assert_eq!(*mvar.0.try_lock().unwrap(), 1);
    }

    #[test]
    fn returns_default_value_when_blocked() {
      let mvar = MVar::new(0);
      let _lock = mvar.0.try_lock().unwrap();
      let result = mvar.try_access(|| 42, |_| panic!("not used"));
      assert_eq!(result, 42);
    }
  }

  mod iter_cloned {
    use super::*;

    #[test]
    fn allows_to_iterate_over_contained_vectors() {
      let mvar = MVar::new(vec![1, 2, 3]);
      let result = mvar.iter_cloned().collect::<Vec<i64>>();
      assert_eq!(result, vec![1, 2, 3]);
    }

    #[test]
    fn iterates_over_elements_added_during_iteration() {
      let mvar = MVar::new(vec![1]);
      let mut iter = mvar.iter_cloned();
      assert_eq!(iter.next(), Some(1));
      mvar.access(|vec| vec.push(2));
      assert_eq!(iter.next(), Some(2));
      assert_eq!(iter.next(), None);
    }
  }
}
