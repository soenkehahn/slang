use std::iter::FromIterator;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Map<Key, Value>(Vec<(Key, Value)>);

impl<Key, Value> FromIterator<(Key, Value)> for Map<Key, Value> {
  fn from_iter<I: IntoIterator<Item = (Key, Value)>>(iter: I) -> Map<Key, Value> {
    Map(iter.into_iter().collect())
  }
}

impl<Key, Value> Map<Key, Value>
where
  Key: PartialEq,
{
  pub fn new() -> Map<Key, Value> {
    Map(Vec::new())
  }

  pub fn insert(&mut self, key: Key, value: Value) {
    self.remove_entry(&key);
    self.0.push((key, value));
  }

  pub fn get(&self, wanted: &Key) -> Option<&Value> {
    for (key, value) in self.0.iter().rev() {
      if key == wanted {
        return Some(value);
      }
    }
    None
  }

  pub fn remove_entry(&mut self, key: &Key) -> Option<(Key, Value)> {
    match self.0.iter().position(|(k, _)| k == key) {
      Some(index) => Some(self.0.remove(index)),
      None => None,
    }
  }

  pub fn iter(&self) -> impl Iterator<Item = (&Key, &Value)> {
    self.0.iter().map(|(a, b)| (a, b))
  }

  pub fn into_iter(self) -> impl Iterator<Item = (Key, Value)> {
    self.0.into_iter()
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use std::hash::{Hash, Hasher};
  use std::{collections::hash_map::DefaultHasher, iter::once};

  #[test]
  fn empty() {
    let map: Map<String, ()> = Map::new();
    assert_eq!(map.get(&"foo".to_string()), None);
  }

  #[test]
  fn stores_values() {
    let mut map: Map<String, i32> = Map::new();
    map.insert("foo".to_string(), 42);
    assert_eq!(map.get(&"foo".to_string()), Some(&42));
  }

  #[test]
  fn insert_overwrites_existing_values() {
    let mut map: Map<String, i32> = Map::new();
    map.insert("foo".to_string(), 42);
    map.insert("foo".to_string(), 23);
    assert_eq!(map.get(&"foo".to_string()), Some(&23));
  }

  mod remove_entry {
    use super::*;

    #[test]
    fn missing_values() {
      let mut map: Map<String, i32> = Map::new();
      assert_eq!(map.remove_entry(&"foo".to_string()), None);
    }

    #[test]
    fn removes_existing_values() {
      let mut map: Map<String, i32> = Map::new();
      map.insert("foo".to_string(), 42);
      assert_eq!(
        map.remove_entry(&"foo".to_string()),
        Some(("foo".to_string(), 42))
      );
      assert_eq!(map.get(&"foo".to_string()), None);
    }
  }

  #[test]
  fn iter() {
    let mut map: Map<String, i32> = Map::new();
    map.insert("foo".to_string(), 42);
    map.insert("bar".to_string(), 23);
    assert_eq!(
      map.iter().collect::<Vec<_>>(),
      vec![(&"foo".to_string(), &42), (&"bar".to_string(), &23)]
    );
  }

  #[test]
  fn iter_iterates_through_elements_in_insertion_order() {
    let mut map: Map<String, i32> = Map::new();
    for char in 'a'..='z' {
      map.insert(char.to_string(), 0);
    }
    assert_eq!(
      map.iter().map(|(key, _)| key.clone()).collect::<Vec<_>>(),
      ('a'..='z').map(|char| char.to_string()).collect::<Vec<_>>()
    );
  }

  #[test]
  fn iter_excludes_overwritten_entries() {
    let mut map: Map<String, i32> = Map::new();
    map.insert("foo".to_string(), 42);
    map.insert("foo".to_string(), 23);
    assert_eq!(
      map.iter().collect::<Vec<_>>(),
      vec![(&"foo".to_string(), &23)]
    )
  }

  #[test]
  fn into_iter() {
    let mut map: Map<String, String> = Map::new();
    map.insert("foo".to_string(), "foo".to_string());
    map.insert("bar".to_string(), "bar".to_string());
    assert_eq!(
      map.into_iter().collect::<Vec<_>>(),
      vec![
        ("foo".to_string(), "foo".to_string()),
        ("bar".to_string(), "bar".to_string())
      ]
    );
  }

  #[test]
  fn into_iter_iterates_through_elements_in_insertion_order() {
    let mut map: Map<String, i32> = Map::new();
    for char in 'a'..='z' {
      map.insert(char.to_string(), 0);
    }
    assert_eq!(
      map.into_iter().map(|(key, _)| key).collect::<Vec<_>>(),
      ('a'..='z').map(|char| char.to_string()).collect::<Vec<_>>()
    );
  }

  #[test]
  fn collect_into_map() {
    let map: Map<String, i32> = once(("foo".to_string(), 42)).collect();
    assert_eq!(map.get(&"foo".to_string()), Some(&42));
  }

  #[test]
  fn is_hashable() {
    let hasher = &mut DefaultHasher::new();
    let map: Map<String, i32> = Map::new();
    map.hash(hasher);
  }

  #[test]
  fn hash_is_value_based() {
    let mut map: Map<i32, i32> = Map::new();
    map.insert(0, 42);
    let a = {
      let hasher = &mut DefaultHasher::new();
      map.hash(hasher);
      hasher.finish()
    };
    map.insert(0, 23);
    let b = {
      let hasher = &mut DefaultHasher::new();
      map.hash(hasher);
      hasher.finish()
    };
    assert_ne!(a, b);
  }
}
