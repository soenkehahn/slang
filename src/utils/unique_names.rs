use std::collections::HashMap;
use std::hash::Hash;

pub struct UniqueNames<T> {
  next_char: char,
  turn: i128,
  map: HashMap<T, String>,
}

impl<T> UniqueNames<T>
where
  T: Eq + Hash,
{
  pub fn new() -> UniqueNames<T> {
    UniqueNames {
      next_char: 'a',
      turn: 1,
      map: HashMap::new(),
    }
  }

  pub fn new_name(&mut self) -> String {
    let mut result = self.next_char.to_string();
    if self.turn > 1 {
      result.push_str(&self.turn.to_string());
    }
    if self.next_char == 'z' {
      self.next_char = 'a';
      self.turn += 1;
    } else {
      self.next_char = std::char::from_u32(self.next_char as u32 + 1).unwrap();
    }
    result
  }

  pub fn get_name(&mut self, t: T) -> String {
    match self.map.get(&t) {
      None => {
        let new = self.new_name();
        self.map.insert(t, new.clone());
        new
      }
      Some(name) => name.clone(),
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use std::collections::HashSet;

  struct TestIterator(i64, UniqueNames<i64>);

  impl Iterator for TestIterator {
    type Item = String;

    fn next(&mut self) -> Option<String> {
      let n = self.0;
      self.0 += 1;
      Some(self.1.get_name(n))
    }
  }

  impl TestIterator {
    fn new() -> TestIterator {
      TestIterator(0, UniqueNames::new())
    }
  }

  #[test]
  fn generates_lower_case_letters_alphabetically() {
    let alphabet: Vec<String> = ('a'..='z').map(|char| char.to_string()).collect();
    let result: Vec<String> = TestIterator::new().take(alphabet.len()).collect();
    assert_eq!(alphabet, result);
  }

  #[test]
  fn generates_longer_names_afterwards() {
    let result: Vec<String> = TestIterator::new().skip(25).take(4).collect();
    assert_eq!(result, vec!["z", "a2", "b2", "c2"]);
    let result: Vec<String> = TestIterator::new().skip(26 * 2 - 1).take(4).collect();
    assert_eq!(result, vec!["z2", "a3", "b3", "c3"]);
  }

  #[test]
  fn generates_unique_names() {
    let n = 2000;
    let names: HashSet<String> = TestIterator::new().take(n).collect();
    assert_eq!(names.len(), n);
  }

  #[test]
  fn returns_the_same_names_for_identical_items() {
    let mut names = UniqueNames::new();
    assert_eq!(names.get_name(42), names.get_name(42));
  }
}
