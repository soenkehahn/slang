mod comments;
pub mod name_resolution;
pub mod parser;
mod tokenizer;
pub mod type_checker;

pub use crate::frontend::name_resolution::AstNode;
pub use crate::frontend::type_checker::{Ast, Type};
use crate::utils::errors::R;

pub fn run(debug: bool, code: &str) -> R<Ast<Type>> {
  type_checker::run(
    debug,
    name_resolution::run(parser::run(tokenizer::run(code))?)?,
  )
}
