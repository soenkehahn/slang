use crate::frontend::type_checker::internal_type::InternalType;
use crate::utils::errors::R;
use crate::utils::map::Map;
use crate::{
  interpreter::{Ast, Interpreter},
  utils::pretty::Pretty,
};
use indexmap::IndexMap;
use std::sync::Arc;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum BuiltinName {
  Println,
  Concat,
}

impl Pretty for BuiltinName {
  fn pretty(&self) -> String {
    match self {
      BuiltinName::Println => "println",
      BuiltinName::Concat => "concat",
    }
    .to_string()
  }
}

pub fn resolve_builtin(name: &str) -> Option<BuiltinName> {
  match name {
    "println" => Some(BuiltinName::Println),
    "concat" => Some(BuiltinName::Concat),
    _ => None,
  }
}

pub fn builtin_type(builtin_name: &BuiltinName) -> InternalType {
  match builtin_name {
    BuiltinName::Println => InternalType::Function(
      Box::new(InternalType::String),
      Box::new(InternalType::Object(Map::new())),
    ),
    BuiltinName::Concat => InternalType::Function(
      Box::new(InternalType::String),
      Box::new(InternalType::Function(
        Box::new(InternalType::String),
        Box::new(InternalType::String),
      )),
    ),
  }
}

#[derive(Debug)]
#[cfg_attr(test, derive(PartialEq))]
pub struct RuntimeValue {
  pub builtin_name: BuiltinName,
  pub applied_argument: Option<Arc<Ast>>,
}

impl RuntimeValue {
  pub fn new(builtin_name: BuiltinName) -> RuntimeValue {
    RuntimeValue {
      builtin_name,
      applied_argument: None,
    }
  }
}

impl<'a> Interpreter<'a> {
  pub fn evaluate_builtin(
    &mut self,
    runtime_value: &RuntimeValue,
    new_argument: Arc<Ast>,
  ) -> R<Ast> {
    Ok(match runtime_value.builtin_name {
      BuiltinName::Println => {
        writeln!(self.stdout, "{}", new_argument.expect_string())?;
        Ast::Object {
          template: false,
          map: IndexMap::new(),
        }
      }
      BuiltinName::Concat => match &runtime_value.applied_argument {
        None => Ast::BuiltinRuntimeValue(RuntimeValue {
          builtin_name: BuiltinName::Concat,
          applied_argument: Some(new_argument),
        }),
        Some(first_argument) => Ast::String(format!(
          "{}{}",
          first_argument.expect_string(),
          new_argument.expect_string()
        )),
      },
    })
  }
}
