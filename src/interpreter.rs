mod objects;
mod reference;
#[cfg(test)]
mod test;

use crate::interpreter::objects::ObjectEvaluator;
use crate::utils::errors::R;
use crate::{builtins, frontend::type_checker};
use crate::{
  frontend::{self, Type},
  utils::pretty::{render_object, Pretty},
};
use builtins::RuntimeValue;
use indexmap::IndexMap;
use objects::BoundFieldIdentifier;
use reference::Reference;
use std::iter::once;
use std::string;
use std::sync::Arc;
use std::{collections::HashMap, io::Write};
use Ast::*;

pub fn run(debug: bool, stdout: &mut dyn Write, ast: frontend::Ast<Type>) -> R<i32> {
  let value = Interpreter::new(stdout, None).evaluate(Ast::new(ast))?;
  if debug {
    eprintln!("RESULT:\n{}", value.pretty());
  }
  Ok(0)
}

fn type_checker_bug<T>() -> T {
  panic!("type checker bug")
}

#[derive(Debug)]
#[cfg_attr(test, derive(PartialEq))]
pub enum Ast {
  Lambda(string::String, Arc<Ast>),
  Application(Arc<Ast>, Arc<Ast>),
  Object {
    template: bool,
    map: IndexMap<string::String, Arc<Ast>>,
  },
  BoundFieldIdentifier(BoundFieldIdentifier),
  Accessor(Arc<Ast>, string::String),
  UnboundIdentifier(string::String),
  String(string::String),
  BuiltinRuntimeValue(builtins::RuntimeValue),
  IntersectionCons {
    argument_type: Arc<type_checker::Type>,
    function: Arc<Ast>,
    rest: Arc<Ast>,
  },
}

impl Pretty for Ast {
  fn pretty(&self) -> string::String {
    match self {
      Lambda(parameter, body) => format!("(\\ {} . {})", parameter, body.pretty()),
      UnboundIdentifier(identifier) => identifier.clone(),
      BoundFieldIdentifier(bound_field_identifier) => bound_field_identifier.name.clone(),
      String(string) => format!("\"{}\"", string),
      Object { template: _, map } => render_object(" =", map.iter()),
      Accessor(expression, accessor) => format!("({}.{})", expression.pretty(), accessor),
      BuiltinRuntimeValue(RuntimeValue {
        builtin_name,
        applied_argument: None,
      }) => builtin_name.pretty(),
      BuiltinRuntimeValue(RuntimeValue {
        builtin_name,
        applied_argument: Some(argument),
      }) => format!("{} {}", builtin_name.pretty(), argument.pretty()),
      Application(a, b) => format!("({} {})", a.pretty(), b.pretty()),
      IntersectionCons {
        argument_type: _,
        function,
        rest,
      } => format!("({}) & ({})", function.pretty(), rest.pretty()),
    }
  }
}

impl Ast {
  pub fn new(ast: frontend::Ast<Type>) -> Arc<Ast> {
    match ast.ast {
      frontend::AstNode::Lambda {
        parameter,
        parameter_type: _,
        body,
      } => Arc::new(Lambda(parameter.name, Ast::new(*body))),
      frontend::AstNode::Application(a, b) => Arc::new(Application(Ast::new(*a), Ast::new(*b))),
      frontend::AstNode::Object(map) => Arc::new(Object {
        template: true,
        map: map
          .into_iter()
          .map(|(key, value)| (key.name, Ast::new(value)))
          .collect(),
      }),
      frontend::AstNode::Accessor(object, accessor) => {
        Arc::new(Accessor(Ast::new(*object), accessor))
      }
      frontend::AstNode::Identifier(id) => Arc::new(UnboundIdentifier(id.name)),
      frontend::AstNode::StringLiteral(string) => Arc::new(String(string)),
      frontend::AstNode::Builtin(builtin_name) => Arc::new(BuiltinRuntimeValue(
        builtins::RuntimeValue::new(builtin_name),
      )),
      frontend::AstNode::Typed(expression, _type) => Ast::new(*expression),
      frontend::AstNode::Intersection(a, rest) => match a.typ.clone() {
        Type::Function(a_input_type, _) => Arc::new(IntersectionCons {
          argument_type: Arc::new(*a_input_type),
          function: Ast::new(*a),
          rest: Ast::new(*rest),
        }),
        _ => type_checker_bug(),
      },
    }
  }

  pub fn expect_string(&self) -> &str {
    match self {
      String(string) => string,
      _ => type_checker_bug(),
    }
  }

  fn clone_and_replace_unbound_identifiers(
    needles: &IndexMap<string::String, Arc<Ast>>,
    haystack: Arc<Ast>,
  ) -> Arc<Ast> {
    match &*haystack {
      UnboundIdentifier(name) => match needles.get(name) {
        Some(replacement) => replacement.clone(),
        None => haystack,
      },
      Lambda(name, body) => {
        if needles.contains_key(name) {
          let mut needles = needles.clone();
          needles.remove(name);
          Arc::new(Lambda(
            name.clone(),
            Ast::clone_and_replace_unbound_identifiers(&needles, body.clone()),
          ))
        } else {
          Arc::new(Lambda(
            name.clone(),
            Ast::clone_and_replace_unbound_identifiers(needles, body.clone()),
          ))
        }
      }
      Object { template, map } => {
        let mut needles = needles.clone();
        for name in map.keys() {
          needles.remove(name);
        }
        Arc::new(Object {
          template: *template,
          map: map
            .iter()
            .map(|(name, value)| {
              (
                name.clone(),
                Ast::clone_and_replace_unbound_identifiers(&needles, value.clone()),
              )
            })
            .collect(),
        })
      }
      Application(a, b) => Arc::new(Application(
        Ast::clone_and_replace_unbound_identifiers(needles, a.clone()),
        Ast::clone_and_replace_unbound_identifiers(needles, b.clone()),
      )),
      Accessor(object, accessor) => Arc::new(Accessor(
        Ast::clone_and_replace_unbound_identifiers(needles, object.clone()),
        accessor.clone(),
      )),
      BoundFieldIdentifier(_) => haystack,
      String(_) => haystack,
      BuiltinRuntimeValue { .. } => haystack,
      IntersectionCons {
        argument_type,
        function,
        rest,
      } => Arc::new(IntersectionCons {
        argument_type: argument_type.clone(),
        function: Ast::clone_and_replace_unbound_identifiers(needles, function.clone()),
        rest: Ast::clone_and_replace_unbound_identifiers(needles, rest.clone()),
      }),
    }
  }

  fn is_of_type(&self, typ: &Type) -> bool {
    self.is_of_type_helper(typ, &HashMap::new())
  }

  fn is_of_type_helper(&self, typ: &Type, recursive_types: &HashMap<string::String, Type>) -> bool {
    match typ {
      Type::String => matches!(self, String(_)),
      Type::Object(type_map) => match self {
        Object { template: _, map } => {
          for (key, field_type) in type_map {
            match map.get(key) {
              Some(field) => {
                if !field.is_of_type_helper(field_type, recursive_types) {
                  return false;
                }
              }
              None => return false,
            }
          }
          true
        }
        _ => false,
      },
      Type::Union(a, b) => {
        self.is_of_type_helper(a, recursive_types) || self.is_of_type_helper(b, recursive_types)
      }
      Type::Intersection(a, b) => {
        self.is_of_type_helper(a, recursive_types) && self.is_of_type_helper(b, recursive_types)
      }
      Type::Recursive(variable, inner_type) => {
        let mut recursive_types = recursive_types.clone();
        recursive_types.insert(variable.clone(), *inner_type.clone());
        self.is_of_type_helper(inner_type, &recursive_types)
      }
      Type::RecursiveVariable(variable) => {
        self.is_of_type_helper(recursive_types.get(variable).unwrap(), recursive_types)
      }
      Type::Top => true,
      Type::Bottom => false,
      Type::Function(_, _) | Type::Variable(_) => type_checker_bug(),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Copy, PartialOrd)]
pub enum Status {
  Evaluated,
  Unevaluated,
}

pub struct Interpreter<'a> {
  pub stdout: &'a mut dyn Write,
  step_counter: i128,
  max_steps: Option<i128>,
}

impl<'a> Interpreter<'a> {
  fn new(stdout: &mut dyn Write, max_steps: Option<i128>) -> Interpreter {
    Interpreter {
      stdout,
      step_counter: 0,
      max_steps,
    }
  }

  fn increase_step_counter(&mut self) -> R<()> {
    if let Some(max_steps) = self.max_steps {
      self.step_counter += 1;
      if self.step_counter > max_steps {
        return Err("non-terminating".into());
      }
    }
    Ok(())
  }

  fn evaluate(&mut self, ast: Arc<Ast>) -> R<Arc<Ast>> {
    let (status, result) = self.evaluate_helper(ast)?;
    match status {
      Status::Unevaluated => self.evaluate(result),
      Status::Evaluated => Ok(result),
    }
  }

  fn evaluate_helper(&mut self, ast: Arc<Ast>) -> R<(Status, Arc<Ast>)> {
    self.increase_step_counter()?;
    Ok(match &*ast {
      Application(function, argument) => {
        let (status, function) = self.evaluate_helper(function.clone())?;
        if status == Status::Unevaluated {
          return Ok((Status::Unevaluated, ast));
        }
        let (status, argument) = self.evaluate_helper(argument.clone())?;
        if status == Status::Unevaluated {
          return Ok((Status::Unevaluated, ast));
        }
        match &*function {
          BuiltinRuntimeValue(runtime_value) => (
            Status::Evaluated,
            Arc::new(self.evaluate_builtin(runtime_value, argument)?),
          ),
          Lambda(name, body) => {
            let parameter_replacement = once((name.clone(), argument)).collect();
            let body =
              Ast::clone_and_replace_unbound_identifiers(&parameter_replacement, body.clone());
            self.evaluate_helper(body)?
          }
          IntersectionCons {
            argument_type,
            function,
            rest,
          } => self.evaluate_intersection_cons(argument_type, function, &argument, rest)?,
          _ => type_checker_bug(),
        }
      }

      Object {
        template: true,
        map,
      } => ObjectEvaluator::new(map).run(self)?,

      Accessor(object, accessor) => {
        let (status, object) = self.evaluate_helper(object.clone())?;
        if status == Status::Unevaluated {
          return Ok((Status::Unevaluated, ast));
        }
        match &*object {
          Object {
            template: false,
            map,
          } => match map.get(accessor) {
            Some(field) => (Status::Evaluated, field.clone()),
            None => type_checker_bug(),
          },
          _ => type_checker_bug(),
        }
      }

      BoundFieldIdentifier(bound_field_identifier) => {
        ObjectEvaluator::evaluate_bound_field_identifier(&ast, bound_field_identifier)?
      }

      IntersectionCons {
        argument_type,
        function,
        rest,
      } => {
        let (status, function) = self.evaluate_helper(function.clone())?;
        if status == Status::Unevaluated {
          return Ok((
            Status::Unevaluated,
            Arc::new(IntersectionCons {
              argument_type: argument_type.clone(),
              function,
              rest: rest.clone(),
            }),
          ));
        }
        let (status, rest) = self.evaluate_helper(rest.clone())?;
        if status == Status::Unevaluated {
          // this is untested
          return Ok((
            Status::Unevaluated,
            Arc::new(IntersectionCons {
              argument_type: argument_type.clone(),
              function,
              rest,
            }),
          ));
        }
        (
          Status::Evaluated,
          Arc::new(IntersectionCons {
            argument_type: argument_type.clone(),
            function,
            rest,
          }),
        )
      }

      Object {
        template: false, ..
      }
      | BuiltinRuntimeValue { .. }
      | String(..)
      | Lambda(..) => (Status::Evaluated, ast),

      UnboundIdentifier(..) => panic!("interpreter bug"),
    })
  }

  fn evaluate_intersection_cons(
    &mut self,
    argument_type: &Type,
    function: &Arc<Ast>,
    argument: &Arc<Ast>,
    rest: &Arc<Ast>,
  ) -> R<(Status, Arc<Ast>)> {
    if argument.is_of_type(argument_type) {
      let application = Ast::Application(function.clone(), argument.clone());
      self.evaluate_helper(Arc::new(application))
    } else {
      let application = Ast::Application(rest.clone(), argument.clone());
      self.evaluate_helper(Arc::new(application))
    }
  }
}
