use crate::interpreter::Ast;
use crate::interpreter::Interpreter;
use crate::interpreter::Reference;
use crate::interpreter::Status;
use crate::R;
use indexmap::IndexMap;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::sync::Arc;

type IdentifierReference = Reference<Option<Arc<Ast>>>;

#[derive(Debug, Clone)]
#[cfg_attr(test, derive(PartialEq))]
pub struct BoundFieldIdentifier {
  pub name: String,
  pub queue: Reference<FieldEvaluationQueue>,
  pub bound_value: IdentifierReference,
}

#[derive(Debug)]
#[cfg_attr(test, derive(PartialEq, Eq))]
pub struct FieldEvaluationQueue {
  pub current: Option<String>,
  pub queue: VecDeque<String>,
}

impl Iterator for Reference<FieldEvaluationQueue> {
  type Item = String;

  fn next(&mut self) -> Option<String> {
    let mut lock = self
      .lock()
      .expect("Reference<FieldEvaluationQueue> is poisoned");
    match lock.queue.pop_front() {
      Some(name) => {
        lock.current = Some(name.clone());
        Some(name)
      }
      None => None,
    }
  }
}

impl FieldEvaluationQueue {
  fn new(map: &IndexMap<String, Arc<Ast>>) -> Reference<FieldEvaluationQueue> {
    Reference::new(FieldEvaluationQueue {
      current: None,
      queue: map.keys().cloned().collect(),
    })
  }
}

pub struct ObjectEvaluator {
  queue: Reference<FieldEvaluationQueue>,
  map: IndexMap<String, (Arc<Ast>, IdentifierReference)>,
  replacements: IndexMap<String, Arc<Ast>>,
}

impl Iterator for ObjectEvaluator {
  type Item = (String, Arc<Ast>, IdentifierReference);

  fn next(&mut self) -> Option<Self::Item> {
    match self.queue.next() {
      Some(name) => {
        let (field, reference) = self
          .map
          .get(&name)
          .expect("Interpreter::evaluate: key mismatch")
          .clone();
        Some((name, field, reference))
      }
      None => None,
    }
  }
}

impl ObjectEvaluator {
  pub fn new(object: &IndexMap<String, Arc<Ast>>) -> ObjectEvaluator {
    let queue = FieldEvaluationQueue::new(object);
    let map: IndexMap<String, (Arc<Ast>, IdentifierReference)> = object
      .iter()
      .map(|(key, value)| (key.clone(), (value.clone(), Reference::new(None))))
      .collect();
    let replacements = map
      .iter()
      .map(|(key, reference)| {
        (
          key.clone(),
          Arc::new(Ast::BoundFieldIdentifier(BoundFieldIdentifier {
            name: key.clone(),
            queue: queue.clone(),
            bound_value: reference.1.clone(),
          })),
        )
      })
      .collect();
    ObjectEvaluator {
      queue,
      map,
      replacements,
    }
  }

  pub fn run(&mut self, interpreter: &mut Interpreter) -> R<(Status, Arc<Ast>)> {
    let mut fields: IndexMap<String, Arc<Ast>> = IndexMap::new();
    let mut statuses = HashSet::new();
    while let Some((name, value, reference)) = self.next() {
      let field_with_bound_identifiers =
        Ast::clone_and_replace_unbound_identifiers(&self.replacements, value.clone());
      let (status, evaluated_field) = interpreter.evaluate_helper(field_with_bound_identifiers)?;
      statuses.insert(status);
      if !(self.queue.lock()?.queue.contains(&name)) {
        *reference.lock()? = Some(evaluated_field.clone());
        fields.insert(name.clone(), evaluated_field);
      }
    }
    let status = if statuses.contains(&Status::Unevaluated) {
      Status::Unevaluated
    } else {
      Status::Evaluated
    };
    Ok((
      status,
      Arc::new(Ast::Object {
        template: false,
        map: fields,
      }),
    ))
  }

  pub fn evaluate_bound_field_identifier(
    ast: &Arc<Ast>,
    BoundFieldIdentifier {
      name: _,
      queue,
      bound_value,
    }: &BoundFieldIdentifier,
  ) -> R<(Status, Arc<Ast>)> {
    Ok(match &*bound_value.lock()? {
      Some(bound_ast) => (Status::Evaluated, bound_ast.clone()),
      None => {
        let mut lock = queue.lock()?;
        let referencing_field_name = lock.current.clone().expect("interpreter bug");
        lock.queue.push_back(referencing_field_name);
        (Status::Unevaluated, ast.clone())
      }
    })
  }
}
