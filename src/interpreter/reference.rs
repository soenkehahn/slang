use crate::R;
use std::sync::Arc;
use std::sync::Mutex;
use std::sync::MutexGuard;

#[derive(Debug)]
pub struct Reference<T>(Arc<Mutex<T>>);

impl<T> Clone for Reference<T> {
  fn clone(&self) -> Self {
    Reference(self.0.clone())
  }
}

#[cfg(test)]
impl<T> PartialEq for Reference<T> {
  fn eq(&self, _other: &Reference<T>) -> bool {
    panic!("PartialEq for Queue")
  }
}

impl<T> Reference<T> {
  pub fn new(t: T) -> Reference<T> {
    Reference(Arc::new(Mutex::new(t)))
  }

  pub fn lock(&self) -> R<MutexGuard<T>> {
    self.0.lock().map_err(|err| format!("{}", err).into())
  }
}
