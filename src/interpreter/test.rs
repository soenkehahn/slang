use super::*;
use crate::frontend;
use guard::guard_unwrap;
use pretty_assertions::assert_eq;
use std::io::Cursor;

impl Ast {
  fn expect_object_get(&self, name: &str) -> Arc<Ast> {
    guard_unwrap!(let Object { template, map } = self);
    assert!(!template);
    map.get(name).unwrap().clone()
  }
}

mod is_of_type {
  use super::*;

  #[test]
  fn accepts_strings() {
    assert!(String("foo".to_string()).is_of_type(&Type::String));
  }

  #[test]
  fn rejects_non_strings() {
    assert!(!Object {
      template: false,
      map: IndexMap::new(),
    }
    .is_of_type(&Type::String));
  }

  mod objects {
    use super::*;

    #[test]
    fn accepts_objects() {
      assert!(Object {
        template: false,
        map: IndexMap::new(),
      }
      .is_of_type(&Type::Object(IndexMap::new())));
    }

    #[test]
    fn rejects_non_objects() {
      assert!(!String("".to_string()).is_of_type(&Type::Object(IndexMap::new())));
    }

    #[test]
    fn accepts_correct_object_field_types() {
      assert!(Object {
        template: false,
        map: once(("foo".to_string(), Arc::new(String("".to_string())))).collect(),
      }
      .is_of_type(&Type::Object(
        once(("foo".to_string(), Type::String)).collect()
      )));
    }

    #[test]
    fn rejects_wrong_field_types() {
      assert!(!Object {
        template: false,
        map: once(("foo".to_string(), Arc::new(String("".to_string())))).collect(),
      }
      .is_of_type(&Type::Object(
        once(("foo".to_string(), Type::Object(IndexMap::new()))).collect()
      )));
    }

    #[test]
    fn accepts_subtype_objects() {
      assert!(Object {
        template: false,
        map: once(("foo".to_string(), Arc::new(String("".to_string())))).collect(),
      }
      .is_of_type(&Type::Object(IndexMap::new())));
    }

    #[test]
    fn rejects_non_subtype_objects() {
      assert!(!Object {
        template: false,
        map: IndexMap::new(),
      }
      .is_of_type(&Type::Object(
        once(("foo".to_string(), Type::String)).collect()
      )));
    }
  }

  mod union_types {
    use super::*;

    #[test]
    fn accepts_left_matches() {
      assert!(String("".to_string()).is_of_type(&Type::Union(
        Box::new(Type::String),
        Box::new(Type::Object(IndexMap::new()))
      )));
    }

    #[test]
    fn accepts_right_matches() {
      assert!(String("".to_string()).is_of_type(&Type::Union(
        Box::new(Type::Object(IndexMap::new())),
        Box::new(Type::String),
      )));
    }

    #[test]
    fn rejects_non_matching_values() {
      assert!(
        !Lambda("foo".to_string(), Arc::new(String("".to_string()))).is_of_type(&Type::Union(
          Box::new(Type::Object(IndexMap::new())),
          Box::new(Type::String),
        ))
      );
    }
  }

  mod intersection_types {
    use super::*;

    #[test]
    fn accepts_matching_values() {
      assert!(Object {
        template: false,
        map: vec![
          ("foo".to_string(), Arc::new(String("".to_string()))),
          ("bar".to_string(), Arc::new(String("".to_string())))
        ]
        .into_iter()
        .collect()
      }
      .is_of_type(&Type::Intersection(
        Box::new(Type::Object(
          once(("foo".to_string(), Type::String)).collect()
        )),
        Box::new(Type::Object(
          once(("bar".to_string(), Type::String)).collect()
        )),
      )));
    }

    #[test]
    fn rejects_left_mismatches() {
      assert!(!Object {
        template: false,
        map: IndexMap::new()
      }
      .is_of_type(&Type::Intersection(
        Box::new(Type::Object(
          once(("foo".to_string(), Type::String)).collect()
        )),
        Box::new(Type::Object(IndexMap::new()))
      )));
    }

    #[test]
    fn rejects_right_mismatches() {
      assert!(!Object {
        template: false,
        map: IndexMap::new()
      }
      .is_of_type(&Type::Intersection(
        Box::new(Type::Object(IndexMap::new())),
        Box::new(Type::Object(
          once(("foo".to_string(), Type::String)).collect()
        )),
      )));
    }
  }

  mod non_data_types {
    use super::*;

    #[test]
    #[should_panic(expected = "type checker bug")]
    fn panics_on_function_types() {
      String("".to_string()).is_of_type(&Type::Function(
        Box::new(Type::String),
        Box::new(Type::String),
      ));
    }

    #[test]
    #[should_panic(expected = "type checker bug")]
    fn panics_on_type_variables() {
      String("".to_string()).is_of_type(&Type::Variable("foo".to_string()));
    }
  }

  fn lots_of_values() -> Vec<Ast> {
    vec![
      String("".to_string()),
      Object {
        template: false,
        map: IndexMap::new(),
      },
      Object {
        template: false,
        map: once(("foo".to_string(), Arc::new(String("".to_string())))).collect(),
      },
      Object {
        template: false,
        map: vec![
          ("foo".to_string(), Arc::new(String("".to_string()))),
          ("bar".to_string(), Arc::new(String("".to_string()))),
        ]
        .into_iter()
        .collect(),
      },
      Lambda("foo".to_string(), Arc::new(String("".to_string()))),
    ]
  }

  #[test]
  fn accepts_all_values_for_top() {
    for value in lots_of_values().into_iter() {
      assert!(value.is_of_type(&Type::Top));
    }
  }

  #[test]
  fn rejects_all_values_for_bottom() {
    for value in lots_of_values().into_iter() {
      assert!(!value.is_of_type(&Type::Bottom));
    }
  }
}

fn test(code: &str) -> Arc<Ast> {
  Interpreter::new(&mut Cursor::new(vec![]), Some(10000))
    .evaluate(Ast::new(frontend::run(true, code).unwrap()))
    .unwrap()
}

fn output(code: &str) -> string::String {
  let mut stdout = Cursor::new(vec![]);
  Interpreter::new(&mut stdout, Some(10000))
    .evaluate(Ast::new(frontend::run(true, code).unwrap()))
    .unwrap();
  string::String::from_utf8(stdout.into_inner()).unwrap()
}

#[test]
fn identity_function() {
  assert_eq!(*test("(\\ x . x) \"foo\""), Ast::String("foo".to_string()));
}

#[test]
fn accessors() {
  assert_eq!(
    *test(r#" { foo = "foo"; }.foo "#),
    Ast::String("foo".to_string())
  );
}

#[test]
fn type_ascriptions() {
  assert_eq!(*test(r#" "foo" : string "#), Ast::String("foo".to_string()));
}

#[test]
fn distinguishes_between_variables() {
  assert_eq!(
    *test("(\\ x . \\ y . x) \"x\" \"y\""),
    Ast::String("x".to_string(),)
  );
}

#[test]
fn distinguishes_between_variables_2() {
  assert_eq!(
    *test("(\\ x . \\ y . y) \"x\" \"y\""),
    Ast::String("y".to_string(),)
  );
}

#[test]
fn allows_to_use_functions_twice_with_different_arguments() {
  let ast = test(
    r#"{
      f = \ x . x;
      a = f "a";
      b = f "b";
    }"#,
  );
  assert_eq!(*ast.expect_object_get("a"), Ast::String("a".to_string()));
  assert_eq!(*ast.expect_object_get("b"), Ast::String("b".to_string()));
}

#[test]
fn identifiers_in_multiple_constructs() {
  let ast = test(
    r#"{
      f = \ foo . \ outer_id . {
        in_object = foo;
        application = id "application";
        application_argument = id foo;
        application_function = outer_id "application_function";
        accessor = { x = foo; }.x;
      };
      id = \ x . x;
    }.f "foo" (\ x . x)"#,
  );
  for (field, expected) in &[
    ("in_object", "foo"),
    ("application", "application"),
    ("application_argument", "foo"),
    ("application_function", "application_function"),
    ("accessor", "foo"),
  ] {
    assert_eq!(
      *ast.expect_object_get(field),
      Ast::String(expected.to_string())
    );
  }
}

#[test]
fn strictness() {
  let output = output(
    r#"{
      f = \ x . {
        a = x;
        b = x;
      };
      main = f (println "once");
    }.main"#,
  );
  assert_eq!(&output, "once\n");
}

#[test]
fn field_evaluation_and_identifiers() {
  let ast = test(
    r#"{
      foo = (\ x . x) "foo";
      bar = foo;
    }"#,
  );
  assert_eq!(*ast.expect_object_get("bar"), String("foo".to_string()));
}

#[test]
fn object_siblings_shadow_identifiers() {
  let ast = test(
    r#"{
      foo = \ x . {
        x = "object field";
        y = x;
      };
      bar = (foo "function argument").y;
    }.bar"#,
  );
  assert_eq!(*ast, String("object field".to_string()));
}

#[test]
fn unevaluated_bug() {
  test(
    r#"
      {
        a = \ a . b;
        b = a;
      }
    "#,
  );
}

mod field_references_to_later_fields {
  use super::*;
  use pretty_assertions::assert_eq;

  #[test]
  fn referencing_functions() {
    let ast = test(
      r#"{
        foo = id "string";
        id = \ x . x;
      }.foo"#,
    );
    assert_eq!(*ast, String("string".to_string()));
  }

  #[test]
  fn lambda_arguments() {
    assert_eq!(
      output(
        r#"
          {
            foo = (\ x . println x) string;
            string = "string";
          }.foo
        "#,
      ),
      "string\n".to_string()
    );
  }

  #[test]
  fn accessor_objects() {
    let ast = test(
      r#"{
        main = object.foo;
        object = {
          foo = "foo";
        };
      }.main"#,
    );
    assert_eq!(*ast, String("foo".to_string()));
  }
}

mod function_intersection {
  use super::*;
  use pretty_assertions::assert_eq;

  #[test]
  fn first_function_matches() {
    assert_eq!(
      *test(
        r#"
          {
            f =
              \ x: string . "string" &
              \ x: {} . "object";
            main = f "foo";
          }.main
        "#,
      ),
      String("string".to_string())
    );
  }

  #[test]
  fn second_function_matches() {
    assert_eq!(
      *test(
        r#"
          {
            f =
              \ x: string . "string" &
              \ x: {} . "object";
            main = f {};
          }.main
        "#,
      ),
      String("object".to_string())
    );
  }

  #[test]
  fn third_function_matches() {
    assert_eq!(
      *test(
        r#"
          {
            f =
              \ x: string . "string" &
              \ x: {} . "object" &
              \ f . "catch all"
              ;
            main = f (\ x . x);
          }.main
        "#,
      ),
      String("catch all".to_string())
    );
  }

  #[test]
  fn union_return_types() {
    assert_eq!(
      *test(
        r#"
          {
            f =
              \ x: string . {} &
              \ x: {} . "";
            main = f "";
          }.main
        "#,
      ),
      Object {
        template: false,
        map: IndexMap::new(),
      }
    );
    assert_eq!(
      *test(
        r#"
          {
            f =
              \ x: string . {} &
              \ x: {} . "foo";
            main = f {};
          }.main
        "#,
      ),
      String("foo".to_string())
    );
  }

  #[test]
  fn catch_all_case_works() {
    assert_eq!(
      *test(
        r#"
          {
            f =
              \ x: string . "string" &
              \ x . "catch all";
            main = f { foo = ""; };
          }.main
        "#,
      ),
      String("catch all".to_string())
    );
  }

  #[test]
  fn dispatch_based_on_object_fields() {
    assert_eq!(
      output(
        r#"
          {
            f =
                \ x: { a: string; } . "a"
              & \ x: { b: string; } . "b"
              ;
            main = {
              a = println (concat "a: " (f { a = ""; }));
              b = println (concat "b: " (f { b = ""; }));
            };
          }
        "#,
      ),
      "a: a\nb: b\n".to_string()
    );
  }

  #[test]
  fn dispatch_based_on_object_field_types() {
    assert_eq!(
      output(
        r#"
          {
            f =
                \ x: { a: string; } . "string field"
              & \ x: { a: {}; } . "object field"
              ;
            main = {
              a = println (f { a = ""; });
              b = println (f { a = {}; });
            };
          }
        "#,
      ),
      "string field\nobject field\n".to_string()
    );
  }

  #[test]
  fn dispatch_handles_object_subtyping_correctly() {
    assert_eq!(
      output(
        r#"
          {
            f =
                \ x: { a: string; } . x.a
              & \ x . "catch all"
              ;
            main = println (f { a = "a"; b = "b"; });
          }
        "#,
      ),
      "a\n".to_string()
    );
  }

  #[test]
  fn union_extrusion() {
    assert_eq!(
      *test(
        r#"
          {
            f = {
              result =
                  \ x: string . "string"
                & \ x . "anything";
            }.result;

            main = f "foo";
          }.main
        "#,
      ),
      String("string".to_string())
    );
  }

  #[test]
  fn correct_union_type_dispatch() {
    assert_eq!(
      *test(
        r#"
          {
            f =
                (\ x: string . "a"
              & \ x . "b")
              & \ x: { field: string; } . x.field
              ;
            main = f "";
          }.main
        "#,
      ),
      String("a".to_string())
    );
  }

  #[test]
  fn weird_1() {
    assert_eq!(
      *test(
        r#"
          {
            f =
                \ x: { head: string; } . "a"
              & \ x . "b";
            main = f ((\ h . {
              head = h;
            }) "bar");
          }.main
        "#,
      ),
      String("a".to_string())
    );
  }

  #[test]
  fn intersection_by_identifiers() {
    assert_eq!(
      output(
        r#"
          {
            string_to_string = \ s: string . concat "'" (concat s "'");

            with_a_to_string = \ o .
              concat "{ a = " (
              concat (to_string o.a)
              "; }");

            with_a_and_b_to_string = \ o .
              concat "{ a = " (
              concat (to_string o.a) (
              concat "; b = " (
              concat (to_string o.b)
              "; }")));

            empty_to_string = \ o: {} . "{}";

            to_string =
              with_a_and_b_to_string &
              with_a_to_string &
              string_to_string &
              empty_to_string;

            test = {
              a = "a";
              b = {
                a = {};
              };
            };
            main = {
              a = println (to_string test);
            };
          }
        "#,
      ),
      "{ a = 'a'; b = { a = {}; }; }\n".to_string()
    );
  }

  #[test]
  fn application_on_union_typed_value() {
    assert_eq!(
      *test(
        r#"
          {
            f =
              \ x: string . "string" &
              \ x: {} . "object";
            main = f ("" : string | {});
          }.main
        "#,
      ),
      Ast::String("string".to_string())
    );
  }

  #[test]
  fn uses_only_runtime_values_for_function_dispatch() {
    assert_eq!(
      *test(
        r#"
          {
            f =
              \ x: string . "a" &
              \ x: string | {} . "b";
            main = f ("foo" : string | {});
          }.main
        "#,
      ),
      Ast::String("a".to_string())
    );
  }
}

#[cfg(feature = "proptests")]
mod proptests {
  use super::*;
  use crate::builtins::{BuiltinName, RuntimeValue};
  use crate::frontend::parser::test::proptests::parsed;
  use crate::frontend::{name_resolution, parser};
  use crate::utils::errors::ResultExt;
  use crate::utils::errors::RS;
  use proptest::prelude::*;

  fn type_check(parsed: parser::Ast) -> RS<frontend::Ast<Type>> {
    type_checker::run(true, name_resolution::run(parsed).into_rs()?).into_rs()
  }

  fn evaluate(ast: frontend::Ast<Type>) -> RS<Arc<Ast>> {
    Interpreter::new(&mut Cursor::new(vec![]), Some(10000))
      .evaluate(Ast::new(ast))
      .into_rs()
  }

  fn builtin_type(builtin_name: &BuiltinName) -> Type {
    builtins::builtin_type(builtin_name)
      .coalesce_type()
      .simplify()
  }

  fn expect_return_type(typ: Type) -> Type {
    match typ {
      Type::Function(_, return_type) => *return_type,
      _ => panic!(),
    }
  }

  fn is_subtype(a: &Type, b: &Type) -> bool {
    use Type::*;
    match (a, b) {
      (Function(a, b), Function(x, y)) => is_subtype(x, a) && is_subtype(b, y),
      (String, String) => true,
      (Object(a), Object(b)) => {
        for (key, b_field_type) in b.iter() {
          if let Some(a_field_type) = a.get(key) {
            if !is_subtype(a_field_type, b_field_type) {
              return false;
            }
          } else {
            return false;
          }
        }
        true
      }
      (a, Union(b, c)) => is_subtype(a, b) || is_subtype(a, c),
      _ => {
        eprintln!("is_subtype is false: {:?} !<: {:?}", a, b);
        false
      }
    }
  }

  fn is_of_type(value: Arc<Ast>, typ: &Type) -> bool {
    match (&*value, typ) {
      (Lambda(..), Type::Function(_, _)) => true,
      (IntersectionCons { .. }, Type::Function(_, _)) => true,
      (String(_), Type::String) => true,
      (
        Object {
          template: false,
          map: value_map,
        },
        Type::Object(type_map),
      ) => {
        for (key, field_type) in type_map.iter() {
          match value_map.get(key) {
            Some(field) => {
              if !is_of_type(field.clone(), field_type) {
                return false;
              }
            }
            None => return false,
          }
        }
        true
      }
      (
        BuiltinRuntimeValue(RuntimeValue {
          builtin_name,
          applied_argument,
        }),
        typ,
      ) => {
        let mut builtin_type = builtin_type(builtin_name);
        if applied_argument.is_some() {
          builtin_type = expect_return_type(builtin_type);
        }
        is_subtype(&builtin_type, typ)
      }
      (_, Type::Union(a, b)) => is_of_type(value.clone(), a) || is_of_type(value, b),
      _ => {
        eprintln!("is_of_type is false: {:?} -- {:?}", value, typ);
        false
      }
    }
  }

  proptest! {
    #![proptest_config(
      ProptestConfig{
        max_global_rejects: 10000,
        .. ProptestConfig::default()
      }
    )]

    #[test]
    fn input_ast_can_be_pretty_printed(ast in parsed()) {
      let ast = type_check(ast);
      prop_assume!(ast.is_ok());
      let ast = Ast::new(ast.unwrap());
      ast.pretty();
    }

    #[test]
    fn evaluated_ast_can_be_pretty_printed(ast in parsed()) {
      let ast = type_check(ast);
      prop_assume!(ast.is_ok());
      let ast = ast.unwrap();
      match evaluate(ast) {
        Ok(evaluated) => {
          evaluated.pretty();
        }
        Err(err) if err == "non-terminating" => {}
        _ => panic!()
      }
    }

    #[test]
    fn evaluation_result_has_predicted_type(ast in parsed()) {
      let ast = type_check(ast);
      prop_assume!(ast.is_ok());
      let ast = ast.unwrap();
      let typ = ast.typ.clone();
      match evaluate(ast) {
        Ok(evaluated) => {
          assert!(is_of_type(evaluated, &typ));
        }
        Err(err) if err == "non-terminating" => {}
        _ => panic!()
      }
    }
  }
}
