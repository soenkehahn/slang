ci:
  cd file-tests; cargo test
  cd file-tests; cargo clippy
  cargo test
  cargo clippy --all-targets
  ! grep --line-number 'todo\|fixme\|dbg\!' -r file-tests/src
  ! grep --line-number 'todo\|fixme\|dbg\!\|ignore\|nyi\!' -r src
  just proptests
  cargo run -- example.slang

test-ci-configuration:
  gitlab-runner-linux-amd64 exec docker test

test pattern='':
  cargo test --offline --features=dev -- --test-threads=1 {{pattern}}

proptests pattern='proptests':
  cargo test --features=proptests -- {{pattern}}

proptests-dev pattern='proptests':
  cargo test --features="dev proptests" -- {{pattern}}
