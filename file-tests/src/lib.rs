extern crate proc_macro;

use core::iter::Extend;
use proc_macro::{TokenStream, TokenTree};
use quote::{__private::Ident, format_ident, quote};
use std::collections::HashSet;
use std::fs::read_dir;

#[proc_macro]
pub fn generate_file_tests(input: TokenStream) -> TokenStream {
  match generate_with_result(input) {
    Ok(result) => result,
    Err(err) => panic!("{}", err.to_string()),
  }
}

type R<T> = Result<T, Box<dyn std::error::Error>>;

fn parse_test_function(input: &mut impl Iterator<Item = TokenTree>) -> R<Ident> {
  match input.next() {
    Some(TokenTree::Ident(test_function)) => Ok(format_ident!("{}", test_function.to_string())),
    got => Err(format!("expected: test function name, got: {:?}", got).into()),
  }
}

enum FileGlob {
  All,
  Just(HashSet<String>),
  Ignore(HashSet<String>),
}

impl FileGlob {
  fn matches(&self, file: &str) -> bool {
    match self {
      FileGlob::All => true,
      FileGlob::Just(set) => set.contains(file),
      FileGlob::Ignore(set) => !set.contains(file),
    }
  }
}

fn parse_file_glob(input: &mut impl Iterator<Item = TokenTree>) -> R<FileGlob> {
  let mut result = HashSet::new();
  loop {
    match (input.next(), input.next()) {
      (Some(TokenTree::Punct(punct)), Some(TokenTree::Ident(file))) if punct.as_char() == ',' => {
        result.insert(file.to_string());
      }
      (None, None) => break,
      got => return Err(format!("expected: comma and file name, got: {:?}", got).into()),
    }
  }
  Ok(if result.is_empty() {
    FileGlob::All
  } else if result.contains("ignore") {
    FileGlob::Ignore(result)
  } else {
    FileGlob::Just(result)
  })
}

fn get_test_function(input: TokenStream) -> R<(Ident, FileGlob)> {
  let input = &mut input.into_iter();
  let test_function = parse_test_function(input)?;
  let file_glob = parse_file_glob(input)?;
  Ok((test_function, file_glob))
}

fn generate_with_result(input: TokenStream) -> Result<TokenStream, Box<dyn std::error::Error>> {
  let (test_function_ident, file_glob) = get_test_function(input)?;
  let mut result = TokenStream::new();
  let slang_files = read_dir("file-tests/files")?
    .collect::<Result<Vec<_>, _>>()?
    .into_iter()
    .map(|file| file.path())
    .filter(|dir_entry| dir_entry.extension().filter(|x| *x == "slang").is_some());
  for slang_file in slang_files {
    let test_name = format_ident!(
      "{}",
      slang_file
        .file_stem()
        .ok_or("test file has no stem")?
        .to_os_string()
        .into_string()
        .map_err(|os_str| format!("can't convert OsString: {:?}", os_str))?
    );
    let ignored_attribute = if !file_glob.matches(&test_name.to_string()) {
      quote! { #[ignore] }
    } else {
      quote! {}
    };
    let slang_file_ident = slang_file
      .into_os_string()
      .into_string()
      .map_err(|os_str| format!("can't convert OsString: {:?}", os_str))?;
    let test_tokens = quote! {
        #[test]
        #ignored_attribute
        fn #test_name() -> Result<(), Box<dyn std::error::Error>> {
            #test_function_ident(#slang_file_ident)
        }
    };
    result.extend::<Option<TokenStream>>(Some(test_tokens.into()));
  }
  Ok(result)
}
