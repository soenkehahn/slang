``` bash
cargo run -- example.slang
```

The project is configured to use the `ldd` linker.
(Mostly to speed up compilation time during the edit-compile-test cycle.)
You have to either install `ldd` or you can switch back to the standard linker by deleting `.config/cargo` in the repo.
